﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadPropCtrlr : MonoBehaviour
{
    public string m_roadPropType;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<BulletCtrlr>() != null && m_roadPropType.Equals("Obstacle"))
        {
            StartCoroutine(DestroyedRoadProp());
        }

        if (other.gameObject.tag.Equals("Player"))   //verified if the player has collide with the player
        {
            if(m_roadPropType.Equals("DestructibleBox") && other.transform.parent.GetComponent<CharacterCtrlr>().IsRolling())
            {
                StartCoroutine(DestroyedRoadProp());
            }
            else
            {
                other.transform.parent.GetComponent<CharacterCtrlr>().DecreasePlayerLives();
                StartCoroutine(DestroyedRoadProp());
            }
            //else if (m_roadPropType.Equals("UndestructibleBox") && other.transform.parent.GetComponent<CharacterCtrlr>().IsRolling())
            //{

            //}
        }
    }

    public IEnumerator DestroyedRoadProp()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}
