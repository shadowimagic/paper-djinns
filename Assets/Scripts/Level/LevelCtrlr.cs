﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtils;
public class LevelCtrlr : Singleton<LevelCtrlr>
{

    public LevelStruct m_levelStruct;   //level struct which contains the loaded roads
    public GameObject m_roadsContainer;  //contains the generated road
    public List<ElementStruct> m_availableElements;//List of available elements that could be generated 
    public BossRoadCtrlr m_bossRoadCtrlr;   //controls the boss road

    public void LoadLevel()
    {
        string path = "Levels/Level_1";
        TextAsset json = Resources.Load(path) as TextAsset;
        DestroyCurrentRoadsObj();
        if (json != null)
        {
            m_levelStruct = new LevelStruct();
            m_levelStruct.levelArtsStruct = new List<LevelArtStruct>();
            m_levelStruct = JsonUtility.FromJson<LevelStruct>(json.text);
            //generate the list of roads into the scene
            GenerateRoads();
        }
        else
        {
            Debug.Log("CAN T FIND " + json.text);
        }
    }

    public void DestroyCurrentRoadsObj()
    {
        for (int i = 0; i < m_roadsContainer.transform.childCount; i++)
        {
            Destroy(m_roadsContainer.transform.GetChild(i).gameObject); //destroyed the road child 
        }
    }

    public void GenerateRoads()
    {
        foreach (LevelArtStruct l in m_levelStruct.levelArtsStruct)
        {
            GameObject _elementPrefab = GetSelectedObjBasedOnName(l.elementName);//Generate the gameObject of the clicked element
            GameObject _road = Instantiate(_elementPrefab, m_roadsContainer.transform);
            Destroy(_road.GetComponent<ElementCtrlr>());
            Destroy(_road.GetComponent<BoxCollider>());
            _road.transform.position = l.position;
            _road.transform.rotation = l.rotation;
            if(l.hasEnemies)
            {
                Transform _ringPrefab = _road.transform.Find("RingPrefab");
                _ringPrefab.GetComponent<EnemyConfigs>().m_enemySpeed = l.enemiesSpeed;
                _ringPrefab.GetComponent<EnemyConfigs>().m_numberOfEnemies = l.numberOfEnemies;
                _ringPrefab.GetComponent<EnemyConfigs>().m_isEnemyActivated = l.hasEnemies;
                _ringPrefab.GetComponent<EnemyConfigs>().enabled = true;
            }
            if(l.isLastRoad)
            {
                m_bossRoadCtrlr.m_lastRoad = _road; //saved the last road
            }
            GenerateRoadProps(_road, l);
        }
    }

    public void GenerateRoadProps(GameObject _road, LevelArtStruct _levelArt)
    {
        if(_road.transform.Find("RoadProps") != null)
        {
            GameObject _roadProps = _road.transform.Find("RoadProps").gameObject;
            foreach (RoadPropsStruct r in _levelArt.roadProps)//verified if the current road has road props
            {
                if (r.roadPropsType.Equals("DestructibleBox") || r.roadPropsType.Equals("UndestructibleBox"))
                {
                    foreach (Transform tc in _roadProps.transform.GetChild(0).transform)//verified if box item are activated
                    {
                        if (tc.gameObject.name.ToLower().Equals(r.roadPropsPos.ToLower())) //verified if the GameObject is activated in the hierachy
                        {
                            tc.gameObject.SetActive(true);
                            if(r.roadPropsType.Equals("DestructibleBox"))
                            {
                                tc.GetComponent<RoadPropCtrlr>().m_roadPropType = "DestructibleBox";
                                tc.eulerAngles = new Vector3(tc.eulerAngles.x, 90f, tc.eulerAngles.z);
                            }
                            else
                            {
                                tc.GetComponent<RoadPropCtrlr>().m_roadPropType = "UndestructibleBox";
                                tc.eulerAngles = new Vector3(tc.eulerAngles.x, 0f, tc.eulerAngles.z);
                            }
                        }
                    }
                }
                if (r.roadPropsType.Equals("Obstacle"))
                {
                    foreach (Transform tc in _roadProps.transform.GetChild(1).transform)//verified if box item are activated
                    {
                        if (tc.gameObject.name.ToLower().Equals(r.roadPropsPos.ToLower())) //verified if the GameObject is activated in the hierachy
                        {
                            tc.GetComponent<RoadPropCtrlr>().m_roadPropType = "Obstacle";
                            tc.gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
    }


    public GameObject GetSelectedObjBasedOnName(string _elementName)
    {
        foreach (ElementStruct o in m_availableElements)
        {
            if (o.elementName.Equals(_elementName))
            {
                return o.elementPrefab;
            }
        }
        return null;
    }
}
