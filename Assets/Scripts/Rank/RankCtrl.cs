﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using MyUtils;
public class RankCtrl : Singleton<RankCtrl>
{

    public RankStruct m_rankStruct; //loaded ranks
    public Rank m_currentRank;  //current player rank
    public Image m_rankImage;   //rank image
    public List<Image> m_stars; //stars image
    private string m_rankPath = "Ranks";
    private string m_badgesSpritesPath = "Badge_sprites/";
    public int m_rankIndex;
    public int m_nbrActivatedStars;
    // Start is called before the first frame update
    void Awake()
    {
        m_rankIndex = 0;
        LoadRanks();
        SetPlayerRank(m_rankIndex , 0);
    }

    public void LoadRanks()
    {
        string _ranksPath = m_rankPath + Path.DirectorySeparatorChar + "Ranks";

        TextAsset json = Resources.Load(_ranksPath) as TextAsset;

        if (json != null)
        {
            m_rankStruct = JsonUtility.FromJson<RankStruct>(json.text);
        }
        else
        {
            Debug.LogError("Cannot find file: " + _ranksPath);
        }
    }

    public void IncreasePlayerRank()
    {
        m_rankIndex++;
    }


    public int GetPlayerRankIndex()
    {
        return m_rankIndex;
    }

    public void SetPlayerRank(int _rankIndex, int _activatedStars)
    {
        m_currentRank = m_rankStruct.ranks[_rankIndex];
        m_rankImage.sprite = Resources.Load<Sprite>(m_badgesSpritesPath + m_currentRank.imageName);
        ActivateStar(_activatedStars);
    }

    public void LevelUp(List<Image> _playerHealthBar)
    {
        SetPlayerRank(m_rankIndex, GetNumberOfStarsBasedOnPlayerHealth(_playerHealthBar));
        IncreasePlayerRank();
    }

    public void DesactivateAllStars()
    {
        foreach(Image s in m_stars)
        {
            s.gameObject.SetActive(false);
        }
    }

    public void ActivateStar(int _numberOfStar)
    {
        m_nbrActivatedStars = _numberOfStar;
        for (int i = 0; i < _numberOfStar; i++)
        {
            m_stars[i].gameObject.SetActive(true);
        }
    }

    public int GetNumberOfStarsBasedOnPlayerHealth(List<Image> _playerHealthBar)
    {
        int _remainingBar = 0;
        foreach(Image b in _playerHealthBar)
        {
            if(b.gameObject.activeInHierarchy)
            {
                _remainingBar++;
            }
        }

        if(_remainingBar == _playerHealthBar.Count) //if the player hasn't received damages then we should give three stars
        {
            return 3;
        }
        else if(_remainingBar == 1)
        {
            return 1;
        }
        else if(_remainingBar < _playerHealthBar.Count && _remainingBar != 0)
        {
            return 2;
        }
        return 0;
    }

}


