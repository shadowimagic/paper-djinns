﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCtrlr : MonoBehaviour
{
    public AudioClip m_explodingSound;
    public int m_levelIndex;

    void Start()
    {
        SetBoxMaterial();
    }


    public void SetBoxMaterial()
    {
        GetComponent<MeshRenderer>().sharedMaterial = GameCtrlr.instance.m_pickableObstaclesMaterials[0];
        string obstacleColorTag = GameCtrlr.instance.m_pickableObstaclesMaterials[0].name;
        string[] splitArray = obstacleColorTag.Split(char.Parse("_")); //
        obstacleColorTag = splitArray[1];
        this.gameObject.tag = obstacleColorTag;
        this.gameObject.tag = obstacleColorTag;
        //_obstacle.GetChild(0).GetComponent<ObstacleCtrlr>().SetTag(obstacleColorTag);
    }

    void OnTriggerEnter(Collider collision)
    {
        // Debug.Log("Woooy");
        string thisTag = transform.gameObject.tag;
        // If Bullet entered
        if (collision.gameObject.name == "bullet")
        {
            int bulletLives = collision.transform.parent.GetComponent<BulletCtrlr>().m_lives;
            // If Bullet is of the same color this obstacle
            if (collision.gameObject.tag == thisTag)
            {
                // Get Bullet Lives
                bulletLives--;
                collision.transform.parent.GetComponent<BulletCtrlr>().SetLives(bulletLives);
                // Transform into Coin/Gem
                if (GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
                    GameCtrlr.instance.m_audioSource.PlayOneShot(m_explodingSound);
                DestroyBox();
                //Destroy(transform.gameObject);
                // Debug.Log("Touched by Bro !");
            }
            else // Bullet tag is diff from Obstacle Tag : In this case we directly destroy bullet
            {
                Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab
            }

            if (bulletLives == 0)
            {
                Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab
            }
            // Destroy Bullet  : In any case when collided with a Obstacle
            //Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab

        }
    }

    /// <summary>
    /// Destroyed the current box
    /// </summary>
    public void DestroyBox()
    {
        List<Transform> _bodyParts = transform.parent.GetComponent<BossMovement>().m_bodyParts; //get body parts
        for(int i = 1; i < _bodyParts.Count; i++)
        {
            if(_bodyParts[i].GetComponent<BoxCtrlr>().m_levelIndex == m_levelIndex)//removed the current body part
            {
                _bodyParts.Remove(_bodyParts[i]);   //removed the body part
            }
        }
        Destroy(this.gameObject);
    }
}
