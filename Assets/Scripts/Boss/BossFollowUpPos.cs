﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFollowUpPos : MonoBehaviour
{

    public Rigidbody m_rb;
    Vector3 m_forward = new Vector3(1, 0, 0);
    public float m_moveSpeed = 224;
    bool m_turnLerp = false;
    float m_currentOrientationAngle = -90;
    int m_pos = 0;

    ORIENTATION m_orientation = ORIENTATION.STRAIGHT;
    int m_orientationSide = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!GameCtrlr.instance.m_isCharacterBlinking && !GameCtrlr.instance.IsGamePaused() && GetComponent<Follower>().m_pathCreator == null)
        {
            m_rb.transform.Translate(m_forward * m_moveSpeed * Time.deltaTime);

            if (m_turnLerp)
            {
                this.RotatePlayerLerp();
            }
        }
    }


    void RotatePlayerLerp()
    {
        Quaternion initRotation = m_rb.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0);//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        //Debug.Log(Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(0, m_rb.transform.localEulerAngles.y))) + " - " + m_currentOrientationAngle);
        //if(Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(0, m_rb.transform.localEulerAngles.y))) >= Mathf.Abs(m_currentOrientationAngle) )
        if (Mathf.Approximately(Mathf.Abs(Quaternion.Dot(initRotation, finalRotation)), 1.0f))
        {
            m_pos = 0;
            m_turnLerp = false;
        }

        m_rb.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);

    }

    public float GetCurrentRotationAngle()
    {
        return m_currentOrientationAngle;
    }

    public void TurnPlayer(float angle)
    {
        float angleDiff = angle + m_currentOrientationAngle;
        int side = angleDiff > 0 ? -1 : 1;

        ORIENTATION newOrientation = m_orientation == ORIENTATION.STRAIGHT ? ORIENTATION.TURN : ORIENTATION.STRAIGHT;
        m_orientation = newOrientation;
        m_orientationSide = side;
        m_currentOrientationAngle = angleDiff;
        // Debug.Log("rota : " + angleDiff);
        // RotatePlayer(angleDiff, side);
        StartCoroutine(IToggleTurnLerp());
        // RotatePlayerLerp();
    }

    IEnumerator IToggleTurnLerp()
    {
        m_turnLerp = true;
        yield return 0;
    }


}
