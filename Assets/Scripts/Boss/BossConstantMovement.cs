﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossConstantMovement : MonoBehaviour
{
    Vector3 m_forward = new Vector3(1, 0, 0);

    public float m_moveSpeed = 224;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!GameCtrlr.instance.m_isCharacterBlinking && !GameCtrlr.instance.IsGamePaused())
        {
            GetComponent<Rigidbody>().transform.Translate(m_forward * m_moveSpeed * Time.deltaTime);
        }
    }
}
