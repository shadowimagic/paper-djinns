﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BossMovement : MonoBehaviour
{
    public List<Transform> m_bodyParts = new List<Transform>();
    public float m_minDistance = 0.25f;
    public float m_speed = 1;
    public float m_rotationSpeed = 50f;
    public GameObject m_bodyPrefab;
    private float m_dis;
    private Transform m_curBodyPart;
    private Transform m_prevBodyPart;
    public int m_bossSize;
    public float m_minimalDistanceBodyDist = 20f;
    public bool m_isBossRotating;//bool which indicates if the boss is rotating
    public float m_bossRotationDelay;   //indicates the time we should wait before toggling the boss rotation
    public Vector3 m_bossHeadInitialPos;
    
    void Start()
    {
        m_bossSize = LevelCtrlr.Instance.m_levelStruct.bossStruct.nbrBoxes;
        InitBossSize();
        InvokeRepeating("ToggleBossRotation", m_bossRotationDelay, m_bossRotationDelay);
        m_bossHeadInitialPos = m_bodyParts[0].position; //saved the boss head initial position
    }

    private void Update()
    {
        if(!m_isBossRotating)
        {
            MoveHorizontally(); //if the boss is not activated then we should the body part horizontally
        }
        else
        {
            Move(); //otherwise it should follow the trail
        }
        VerifiedIfLevelCompleted(); //verified if the level completed
    }

    /// <summary>
    /// Verified if the player has completed the current level
    /// </summary>
    public void VerifiedIfLevelCompleted()
    {
        if (m_bodyParts.Count == 1)
        {
            StartCoroutine(IWaitBeforeGameOver());//show end screen
        }
    }


    IEnumerator IWaitBeforeGameOver()
    {
        yield return new WaitForSeconds(0.5f);
        GameCtrlr.instance.GameOver();
    }

    public void InitBossSize()
    {
        for(int i = 0; i < m_bossSize - 1; i++)
        {
            AddBodyPart(i);
        }
    }

    public void ToggleBossRotation()
    {
        m_isBossRotating = !m_isBossRotating;
        if (!m_isBossRotating)
        {
            //StartCoroutine(WaitBeforeReting());
            ResetBossHeadPosition();
            m_bodyParts[0].localEulerAngles = new Vector3(0f, 0f, 0f);
        }
        else
        {
            transform.GetComponentInChildren<BossFollow>().m_distanceTravelled = 0f;//reset the distance travelled
            m_bodyParts[0].localEulerAngles = new Vector3(0f, 90f, 0f);
        }
    }

    IEnumerator WaitBeforeReting()
    {
        yield return new WaitUntil(() => transform.GetChild(0).position.y >= 32f);
    }
    public void ResetBossHeadPosition()
    {
        float _t = Time.deltaTime * m_dis / m_minDistance * m_speed;
        m_bossHeadInitialPos = new Vector3(m_bodyParts[0].GetComponent<BossCtrl>().m_bossInitialPosition.x, m_bodyParts[0].GetComponent<BossCtrl>().m_bossInitialPosition.y, m_bodyParts[0].position.z);
        while (Vector3.Distance(m_bodyParts[0].position, m_bossHeadInitialPos) > 10f)
        {
            m_bodyParts[0].position = Vector3.Slerp(m_bodyParts[0].position, m_bossHeadInitialPos, _t);
        }
    }

    public void MoveHorizontally()
    {
        float _bossXPosition = m_bodyParts[0].transform.position.x;

        for (int i = 1; i < m_bodyParts.Count; i++)
        {
            m_curBodyPart = m_bodyParts[i];
            m_prevBodyPart = m_bodyParts[i - 1];
            m_dis = Vector3.Distance(m_prevBodyPart.position, m_curBodyPart.position);
            Vector3 _newPos = new Vector3(_bossXPosition, m_prevBodyPart.position.y, m_prevBodyPart.position.z + 70f);
            _newPos.y = m_bodyParts[0].position.y + 40f;
            float _t = Time.deltaTime * m_dis / m_minDistance * m_speed;
            if (_t > 0.5f)
                _t = 0.5f;
            m_curBodyPart.position = Vector3.Slerp(m_curBodyPart.position, _newPos, _t);
           // m_curBodyPart.rotation = Quaternion.Slerp(m_curBodyPart.rotation, m_prevBodyPart.rotation, _t);
        }
    }

    public void Move()
    {
        float _curSpeed = m_speed;  //the boss head should always move 
        //m_bodyParts[0].Translate(m_bodyParts[0].forward * _curSpeed * Time.smoothDeltaTime, Space.World);
        for (int i = 1; i < m_bodyParts.Count; i++)
        {
            m_curBodyPart = m_bodyParts[i];
            m_prevBodyPart = m_bodyParts[i-1];
            m_dis = Vector3.Distance(m_prevBodyPart.position, m_curBodyPart.position);
            Vector3 _newPos = m_prevBodyPart.position;
            _newPos.y = m_bodyParts[0].position.y;
            float _t = Time.deltaTime * m_dis / m_minDistance * _curSpeed;
            if (_t > 0.5f)
                _t = 0.5f;
            m_curBodyPart.position = Vector3.Slerp(m_curBodyPart.position, _newPos, _t);
            m_curBodyPart.rotation = Quaternion.Slerp(m_curBodyPart.rotation, m_prevBodyPart.rotation, _t);
        }
    }

    public void AddBodyPart(int _levelIndex)
    {
        Transform _newPart = (Instantiate(m_bodyPrefab, new Vector3(m_bodyParts[m_bodyParts.Count - 1].position.x, m_bodyParts[m_bodyParts.Count - 1].position.y, m_bodyParts[m_bodyParts.Count - 1].position.z + m_minimalDistanceBodyDist), m_bodyParts[m_bodyParts.Count - 1].rotation) as GameObject).transform;
        _newPart.SetParent(transform);
        _newPart.GetComponent<BoxCtrlr>().m_levelIndex = _levelIndex; //added box level index
        m_bodyParts.Add(_newPart);
    }

}
