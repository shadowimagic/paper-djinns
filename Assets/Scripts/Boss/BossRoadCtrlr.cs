﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoadCtrlr : MonoBehaviour
{
    public bool m_hasGeneratedRoad;
    public GameObject m_player;
    public GameObject m_lastRoad;
    public float m_minDistanceReference;
    public LevelStruct m_bossLvlStruct;   //level struct which contains the loaded roads
    public List<ElementStruct> m_availableElements; //List of available elements that could be generated 
    private bool m_hasGeneratedBoss;    //verified if the boss has been generated in the level
    private void Start()
    {
        LoadBossLevel();
    }
    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(m_player.transform.position, m_lastRoad.transform.position) <= m_minDistanceReference)  //if the player has reached the end of the level we must generate the boss road
        {
            m_hasGeneratedRoad = true;  //set the new boss road
            GenerateBossRoad();//generate boss road
            GenerateBoss();//generated the boss when the player has reached the end of the level
        }
    }

    public void GenerateBossRoad()
    {
        GameObject _lastRoad = null;
        int _index = 0;
        foreach (LevelArtStruct l in m_bossLvlStruct.levelArtsStruct)
        {
            GameObject _elementPrefab = GetSelectedObjBasedOnName(l.elementName);//Generate the gameObject of the clicked element
            Quaternion _rot;
            Vector3 _pos;
            if (_index == 0)
            {
                 _pos = GetSelectedPosBasedOnName(l.elementName, m_lastRoad);//get the position of the selected element based of the local position
                 _rot = GetSelectedRotBasedOnName(l.elementName, m_lastRoad);//get the rotation of the selected element based of the local rotation
            }
            else
            {
                 _pos = GetSelectedPosBasedOnName(l.elementName, _lastRoad);//get the position of the selected element based of the local position
                 _rot = GetSelectedRotBasedOnName(l.elementName, _lastRoad);//get the rotation of the selected element based of the local rotation
            }
            GameObject _newElement = Instantiate(_elementPrefab, m_lastRoad.transform.parent.transform);
            Destroy(_newElement.GetComponent<ElementCtrlr>());
            Destroy(_newElement.GetComponent<BoxCollider>());
            _newElement.transform.position = _pos;
            _newElement.transform.rotation = _rot;
            _lastRoad = _newElement;
            _index++;
        }
        m_lastRoad = _lastRoad;
        m_hasGeneratedRoad = false;
    }

    public void GenerateBoss()
    {
        if(!m_hasGeneratedBoss)
        {
            GameCtrlr.instance.GenerateBoss();
            m_hasGeneratedBoss = true;
        }
    }


    public GameObject GetSelectedObjBasedOnName(string _elementName)
    {
        foreach (ElementStruct o in m_availableElements)
        {
            if (o.elementName.Equals(_elementName))
            {
                return o.elementPrefab;
            }
        }
        return null;
    }

    public Vector3 GetSelectedPosBasedOnName(string _elementName, GameObject _ref)
    {
        foreach (Transform t in _ref.transform)
        {
            if (t.gameObject.name.Equals(_elementName))
            {
                return transform.TransformPoint(t.position);
            }
        }
        return new Vector3();
    }


    public Quaternion GetSelectedRotBasedOnName(string _elementName, GameObject _ref)
    {
        foreach (Transform t in _ref.transform)
        {
            if (t.gameObject.name.Equals(_elementName))
            {
                return t.rotation;
            }
        }
        return new Quaternion();
    }

    public void LoadBossLevel()
    {
        string path = "Levels/BossRoad";
        TextAsset json = Resources.Load(path) as TextAsset;
        if (json != null)
        {
            m_bossLvlStruct = new LevelStruct();
            m_bossLvlStruct.levelArtsStruct = new List<LevelArtStruct>();
            m_bossLvlStruct = JsonUtility.FromJson<LevelStruct>(json.text);
        }
        else
        {
            Debug.Log("CAN T FIND " + json.text);
        }
    }
}
