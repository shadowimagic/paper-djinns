﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class BossFollow : MonoBehaviour
{
    public PathCreator m_pathCreator;
    public PathCreator m_entrancePathCreator;   //boss entrance path
    public bool m_hasBossEnterred = true;  //bool val to verified if the boss has enterred the scene
    public float m_speed = 5;
    public float m_distanceTravelled;

    private void Start()
    {
        transform.position = m_entrancePathCreator.path.GetPoint(0);
        transform.parent.parent.position = GetComponent<BossCtrl>().m_bossInitialPosition;
    }
    void FixedUpdate()
    {
        if(m_hasBossEnterred)   //Played the boss entrance movement
        {

            m_distanceTravelled += (m_speed * 2) * Time.deltaTime;
            transform.position = m_entrancePathCreator.path.GetPointAtDistance(m_distanceTravelled, EndOfPathInstruction.Stop);
            if (Vector3.Distance(transform.position, m_entrancePathCreator.path.GetPoint(m_entrancePathCreator.path.NumPoints - 1)) <= 0.001f)
            {
                m_distanceTravelled = 0f;
                m_hasBossEnterred = false;
            }
        }

        if (transform.GetComponentInParent<BossMovement>().m_isBossRotating && !m_hasBossEnterred)
        {
            m_distanceTravelled += m_speed * Time.deltaTime;
            transform.position = m_pathCreator.path.GetPointAtDistance(m_distanceTravelled);
            //transform.rotation = m_pathCreator.path.GetRotationAtDistance(m_distanceTravelled);
        }
    }

    public void TransitionToInitialPosition()
    {
        transform.parent.parent.position = Vector3.Slerp(transform.parent.parent.position, GetComponent<BossCtrl>().m_bossInitialPosition, Time.deltaTime);
        if (Vector3.Distance(transform.parent.parent.position, GetComponent<BossCtrl>().m_bossInitialPosition) <= 0.001f)
        {
            m_distanceTravelled = 0f;
            m_hasBossEnterred = false;
        }
    }
}
