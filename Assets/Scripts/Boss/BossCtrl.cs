﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class BossCtrl : MonoBehaviour
{

    public enum STATE
    {
        ATTACK = 0,
        DEFENCE = 1
    }

    public Transform m_player;
    public float m_followSharpness = 0.1f;
    public Rigidbody m_rb;

    Vector3 m_forward = new Vector3(1, 0, 0);

    public float m_moveSpeed = 224;

    Vector3 m__followOffset;
    float m_currentOrientationAngle = -90;
    int m_pos = 0;
    bool m_turnLerp = false;

    ORIENTATION m_orientation = ORIENTATION.STRAIGHT;
    int m_orientationSide = 1;
    public Canvas m_healthBarCanvas;
    public Boss_struct m_bossCharacteristic;
    private float m_dodgeVal;
    private float m_shootVal;
    public GameObject m_bulletPrefab;
    public Transform m_bulletContainer;
    Material m_bulletMaterial;
    int m_bulletLives = 1;
    STATE m_currentState;
    public AudioClip m_explodingSound;
    public GameObject m_wings;
    //Life bar 
    public List<Image> m_lifeBar;
    int m_currentBossMat = -1;
    public List<HealthState> m_healthState;
    //
    float m_bodyShiftingdelay;
    float m_bulletShiftingdelay;
    const float m_superBulletSize = 0.3f;
    const int m_superBulletLive = 3;
    bool m_isSuperBulletActivated;
	float m_dodgeMaxVal = 20f;
	float m_dodgeValTracker = 0f;
    float m_timeToLive = 0.8f;
    public Vector3 m_bossInitialPosition;

    // Start is called before the first frame update
    void Start()
    {
		m_dodgeValTracker = m_dodgeMaxVal;//init dodge val tracker 
        m_healthBarCanvas.worldCamera = GameObject.Find("Camera").GetComponent<Camera>();
        m_player = GameCtrlr.instance.m_characterCtrlr.gameObject.transform;
        m_pos = m_player.GetComponent<CharacterCtrlr>().GetCurrentPos();
       // Cache the initial offset at time of spawn:
        m__followOffset = transform.position - m_player.position;
        m_bossCharacteristic.nbrBulletsColor = LevelCtrlr.Instance.m_levelStruct.bossStruct.nbrOfBulletColors;
        m_bossCharacteristic.nbrBulletsColor = LevelCtrlr.Instance.m_levelStruct.bossStruct.nbrOfBulletColors;
        Debug.Log(m_bossCharacteristic.actionDelay);
        InvokeRepeating("DoActions", 1f, m_bossCharacteristic.actionDelay);
    }

    public void DoActions()
    {


        ////played the dodge after the boss has shoot
        ///
        if (!GameCtrlr.instance.IsGamePaused())
        {
            m_currentState = GetRandomState();
            if (m_currentState == STATE.DEFENCE && !m_turnLerp)
            {
                Dodge();
            }

            if (m_currentState == STATE.ATTACK && !m_turnLerp)
            {
                Shoot();
            }
        }
    }

    private void Update()
    {
        ManageBossBodyShifting();
        //InvokeRepeating("DoActions", 1f, m_bossCharacteristic.actionDelay);
    }

    //public void FixedUpdate()
    //{
    //    if(!GameCtrlr.instance.m_isCharacterBlinking && !GameCtrlr.instance.IsGamePaused())
    //    {
    //        m_rb.transform.Translate(m_forward * m_moveSpeed * Time.deltaTime);

    //        if (m_turnLerp)
    //        {
    //            this.RotatePlayerLerp();
    //        }
    //    }
    //}

    public void SetRotationAngle(float _rotationAngle)
    {
        m_currentOrientationAngle = _rotationAngle;
    }

    public void FollowUpPlayer()
    {
            // Apply that offset to get a target position.
            Vector3 targetPosition = m_player.position + m__followOffset;

            // Keep our y position unchanged.
            targetPosition.y = transform.position.y;

            // Smooth follow.    
            transform.position += (targetPosition - transform.position) * m_followSharpness;
    }


    void RotatePlayerLerp()
    {
        Quaternion initRotation = m_rb.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0);//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        //Debug.Log(Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(0, m_rb.transform.localEulerAngles.y))) + " - " + m_currentOrientationAngle);
        //if(Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(0, m_rb.transform.localEulerAngles.y))) >= Mathf.Abs(m_currentOrientationAngle) )
        if (Mathf.Approximately(Mathf.Abs(Quaternion.Dot(initRotation, finalRotation)), 1.0f))
        {
            m_pos = 0;
            m_turnLerp = false;
        }

        m_rb.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);

    }

    IEnumerator IToggleTurnLerp()
    {
        m_turnLerp = true;
        yield return 0;
    }

    public void TurnPlayer(float angle)
    {
        float angleDiff = angle + m_currentOrientationAngle;
        int side = angleDiff > 0 ? -1 : 1;

        ORIENTATION newOrientation = m_orientation == ORIENTATION.STRAIGHT ? ORIENTATION.TURN : ORIENTATION.STRAIGHT;
        m_orientation = newOrientation;
        m_orientationSide = side;
        m_currentOrientationAngle = angleDiff;
        // Debug.Log("rota : " + angleDiff);
        // RotatePlayer(angleDiff, side);
        StartCoroutine(IToggleTurnLerp());
        // RotatePlayerLerp();
    }


    public void Dodge()
    {
        if (m_pos == m_player.GetComponent<CharacterCtrlr>().GetCurrentPos())    //if the boss is on the same position as the player
        {
            if (m_pos == -1)//verified if the boss is on the left
            {
                GotoRight();
            }
            else if (m_pos == 1)
            {
                GotoLeft();
            }
            else
            {
                switch (Random.Range(0, 2))
                {
                    case 0:
                        GotoLeft();
                        break;
                    case 1:
                        GotoRight();
                        break;
                }
            }
        }
    }


   public void Shoot()
    {
        if (m_pos != m_player.GetComponent<CharacterCtrlr>().GetCurrentPos())    //if the boss is on the same position as the player
        {
            while (m_pos > m_player.GetComponent<CharacterCtrlr>().GetCurrentPos())
            {
                GotoLeft();
            }
            while (m_pos < m_player.GetComponent<CharacterCtrlr>().GetCurrentPos())
            {
                GotoRight();
            }
            //StartCoroutine(ShootBullet());
            if (!GameCtrlr.instance.m_characterCtrlr.GetTurnLerpValue() && !m_turnLerp)    //shoot if the player is not turning
            {
                GenerateBullet();
            }
        }
        //StartCoroutine(ResetShootingDelay());
    }

    public IEnumerator ShootBullet()
    {
        yield return new WaitForSeconds(5f);
        GenerateBullet();
    }

    public HEALTH_STATE GetBossHealthState()
    {
        int _currentHealthBar = GetRemainingHealthBar();
        if (_currentHealthBar <=  m_healthState.Where(h => h.healthState == HEALTH_STATE.HEALTHY).FirstOrDefault().healthBar 
            && _currentHealthBar > m_healthState.Where(h => h.healthState == HEALTH_STATE.OK).FirstOrDefault().healthBar)
        {
            return HEALTH_STATE.HEALTHY;
        }
        else if (_currentHealthBar <= m_healthState.Where(h => h.healthState == HEALTH_STATE.OK).FirstOrDefault().healthBar
            && _currentHealthBar > m_healthState.Where(h => h.healthState == HEALTH_STATE.HURT).FirstOrDefault().healthBar)
        {
            return HEALTH_STATE.OK;
        }
        else
        {
            return HEALTH_STATE.HURT;
        }
    }

    public void ManageBossBodyShifting()
    {
        if(GetBossHealthState() == HEALTH_STATE.HEALTHY)
        {
            m_bodyShiftingdelay += Time.deltaTime;
            if(m_bodyShiftingdelay >= m_bossCharacteristic.shiftingDelay)
            {
                ShiftBossMaterial();//boss body color should change
                SetBulletMaterial(GetBossMaterial());         //bullet should have the same color
                m_bodyShiftingdelay = 0f;
            }
        }
        else if(GetBossHealthState() == HEALTH_STATE.OK)
        {
            m_bodyShiftingdelay += Time.deltaTime;
            if (m_bodyShiftingdelay >= m_bossCharacteristic.shiftingDelay)
            {
                ShiftBossMaterial();//boss body color should change
                ShiftBulletMaterial();         //bullet should have the same color
                m_bodyShiftingdelay = 0f;
            }
        }
        else
        {
            m_isSuperBulletActivated = true;//shoot big bullet size
        }
    }

    public int GetRemainingHealthBar()
    {
        int _remainingHealthBar = 0;
        for(int i = 0; i < m_lifeBar.Count; i++)
        {
            if(m_lifeBar[i].gameObject.activeInHierarchy)
            {
                _remainingHealthBar++;
            }
        }
        return _remainingHealthBar;
    }


    void GotoRight()
    {
        if (m_pos >= -1 && m_pos < 1 && !m_turnLerp)
        {
            StartCoroutine(ISwipe(1, 9.5f));
            m_pos += 1;
        }
    }

    void GotoLeft()
    {
        if (m_pos <= 1 && m_pos > -1 && !m_turnLerp)
        {
            StartCoroutine(ISwipe(-1, 9.5f));
            m_pos -= 1;
        }
    }


    IEnumerator ISwipe(int _dir = 1, float _max = 8.0f)
    {

        float progress = 0;

        while (progress < _max)
        {
            progress += (1.0f);

            if (_dir == 1)
                m_rb.transform.Translate(new Vector3(0, 0, -1) * progress);

            if (_dir == -1)
                m_rb.transform.Translate(new Vector3(0, 0, 1) * progress);


            if (progress >= _max)
            {
                break;
            }

            yield return 0;
        }

    }

    public void GenerateBullet()
    {
            this.GetComponent<Animator>().SetTrigger("SHOOT");
            GameObject instantiatedBullet = Instantiate(m_bulletPrefab, m_bulletContainer.position, m_bulletPrefab.transform.rotation);
            Material currentMaterial = m_bulletMaterial;
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetTimeToLive(m_timeToLive);
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetAngle(m_currentOrientationAngle);
            string _bulletTag = gameObject.tag + "_b";
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetMaterial(currentMaterial, _bulletTag);
            if(!m_isSuperBulletActivated)
            {
                instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetLives(m_bulletLives);
            }
            else
            {
                instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetLives(m_superBulletLive);
                instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetBulletSize(new Vector3(m_superBulletSize, m_superBulletSize, m_superBulletSize));

            }
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetBulletSpeed(m_bossCharacteristic.bulletSpeed);
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetShootingDirection(Vector3.left);
    }




    public STATE GetRandomState()
    {
        switch(Random.Range(0, 3))
        {
            case 0:
                return STATE.ATTACK;
                break;
            case 1:
                return STATE.ATTACK;
                break;
            case 2:
                return STATE.DEFENCE;
                break;
            default:
                return STATE.ATTACK;
                break;
        }
    }


    public void SetBossMaterial()
    {
        m_currentBossMat = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));
        m_wings.GetComponentInChildren<MeshRenderer>().sharedMaterial = GameCtrlr.instance.m_pickableObstaclesMaterials[m_currentBossMat];
        string obstacleColorTag = GameCtrlr.instance.m_pickableObstaclesMaterials[m_currentBossMat].name;
        string[] splitArray = obstacleColorTag.Split(char.Parse("_")); //
        obstacleColorTag = splitArray[1];
        this.gameObject.tag = obstacleColorTag;
        this.gameObject.tag = obstacleColorTag;
        //_obstacle.GetChild(0).GetComponent<ObstacleCtrlr>().SetTag(obstacleColorTag);
    }

    public Material GetBossMaterial()
    {
        return m_wings.GetComponentInChildren<MeshRenderer>().sharedMaterial;
    }

    public void SetBulletMaterial(Material _mat)
    {
        m_bulletMaterial = _mat;
    }

    public void ShiftBossMaterial()
    {
        int _matIndex = 0;
        _matIndex = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));
        while(_matIndex == m_currentBossMat)
        {
            _matIndex = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));
        }
        m_currentBossMat = _matIndex;
        m_wings.GetComponentInChildren<MeshRenderer>().sharedMaterial = GameCtrlr.instance.m_pickableObstaclesMaterials[m_currentBossMat];
        string obstacleColorTag = GameCtrlr.instance.m_pickableObstaclesMaterials[m_currentBossMat].name;
        string[] splitArray = obstacleColorTag.Split(char.Parse("_")); //
        obstacleColorTag = splitArray[1];
        this.gameObject.tag = obstacleColorTag;
        this.gameObject.tag = obstacleColorTag;
    }

    

    public void ShiftBulletMaterial()
    {
        int _matIndex = 0;
        _matIndex = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));
        while (GameCtrlr.instance.m_pickableObstaclesMaterials[_matIndex].name.Equals(GetBossMaterial().name))
        {
            _matIndex = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));
        }
        m_bulletMaterial = GameCtrlr.instance.m_pickableObstaclesMaterials[_matIndex];
    }



    private void OnTriggerEnter(Collider collision)
    {

        // Debug.Log("Woooy");
        string thisTag = transform.gameObject.tag;
        // If Bullet entered
        if (collision.gameObject.name == "bullet" && !collision.gameObject.tag.Contains("_b"))
        {
            int bulletLives = collision.transform.parent.GetComponent<BulletCtrlr>().m_lives;
            // If Bullet is of the same color this obstacle
            if (collision.gameObject.tag == thisTag)
            {
                // Get Bullet Lives
                bulletLives--;
                collision.transform.parent.GetComponent<BulletCtrlr>().SetLives(bulletLives);
                // Transform into Coin/Gem
                if (GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
                    GameCtrlr.instance.m_audioSource.PlayOneShot(m_explodingSound);
                //GenerateCollect();
                //Destroy(transform.gameObject);
                // Debug.Log("Touched by Bro !");
                DecreaseBossLifeBar();

            }
            else // Bullet tag is diff from Obstacle Tag : In this case we directly destroy bullet
            {
                Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab
            }

            if (bulletLives == 0)
            {
                Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab
            }
            // Destroy Bullet  : In any case when collided with a Obstacle
            //Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab

        }
    }


    public void DecreaseBossLifeBar()
    {
        for (int i = 0; i < m_lifeBar.Count; i++)
        {
            if (m_lifeBar[i].gameObject.activeInHierarchy)
            {
                m_lifeBar[i].gameObject.SetActive(false);
                if(i == m_lifeBar.Count -1)
                {
                    //should destroy boss and show the unlock level
                    GameCtrlr.instance.UnlockNextLevel();
                    RankCtrl.Instance.LevelUp(GameCtrlr.instance.m_characterCtrlr.m_lifeBar);   //increased the player rank
                    GameCtrlr.instance.m_isBossActivated = false;
                    StartCoroutine(AudioManager.Instance.fadeInDefaultSoundtrack());
                    GameCtrlr.instance.m_gameBackground.GetComponent<Animator>().SetTrigger("BOSS_DISAPPEARED");
                    Destroy(this.gameObject);
                }
                return;
            }
        }
    }
}

public enum HEALTH_STATE
{
    HEALTHY,
    OK,
    HURT
}

[System.Serializable]
public class HealthState
{
    public HEALTH_STATE healthState;
    public int healthBar;
}


