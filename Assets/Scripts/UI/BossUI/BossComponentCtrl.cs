﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BossComponentCtrl : MonoBehaviour
{
    public TextMeshProUGUI m_bossNameTxt;
    public Image m_bossImage;
    public List<Image> m_stars;
    public GameObject m_bossUI;
    string fileName;

    public void SetBossComponentInformations(string _bossDisplayName, string _bossFileName, int _numberOfUnlockedStars, GameObject _bossUI, Color _disabledColor)
    {
        fileName = _bossFileName;
        m_bossNameTxt.text = _bossDisplayName;
        m_bossImage.sprite = Resources.Load<Sprite>("Boss_UI/" + _bossFileName);
        if(!DataBackup.Instance.HasUnlockedBoss(_bossDisplayName))
        {
            m_bossImage.color = _disabledColor;
        }
        SetUnlockedStars(_numberOfUnlockedStars);
        m_bossUI = _bossUI;
    }

    public void SetUnlockedStars(int _numberOfUnlockedStars)
    {
        for(int i = 0; i < _numberOfUnlockedStars; i++)
        {
            m_stars[i].gameObject.SetActive(true);
        }
    }

    public void ToggleBossDisplay()
    {
        m_bossUI.GetComponent<BossUICtrl>().RenderBoss(m_bossNameTxt.text, m_stars, fileName);
    }
}
