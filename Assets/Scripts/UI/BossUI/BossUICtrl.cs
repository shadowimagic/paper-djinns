﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BossUICtrl : MonoBehaviour
{
    public GameObject m_menuScreen;
    public GameObject m_bossScreen;
    public GameObject m_bossComponentPrefab;
    public GameObject m_container;  //the container of boss components
    public List<Image> m_bossRendererStars;
    public TextMeshProUGUI m_bossNameRender;
    public GameObject m_bossRenderer;
    public GameObject m_bossListDisplay;
    public GameObject m_bossPosition;
    public GameObject m_currentRenderBoss;
    public Color m_desactivatedColor;
    public void PopulateBossUIContainer()
    {
        if(m_container.transform.childCount == 0)//verified if the container already had boss components
        {
            foreach(Gameplay_level_struct l in GameCtrlr.instance.GetListOfLevels())
            {
               GameObject _bossComponent =  Instantiate(m_bossComponentPrefab, m_container.transform);
                _bossComponent.GetComponent<BossComponentCtrl>().SetBossComponentInformations(l.boss.displayName, l.boss.fileName, DataBackup.Instance.GetEarnedStarsOnBossFight(l.boss.displayName), gameObject, m_desactivatedColor);
            }
        }
    }

    public void RenderBoss(string _name, List<Image> _stars, string _bossFileName)
    {
        ToggleBossRenderDisplay(true);
        m_bossNameRender.text = _name;
        Debug.Log("file " + _bossFileName);
        m_currentRenderBoss = Instantiate(Resources.Load("Bosses/"+ _bossFileName+""), m_bossPosition.transform /*, _player.transform.parent*/) as GameObject;
        m_currentRenderBoss.layer = LayerMask.NameToLayer("Boss_render");
        m_currentRenderBoss.transform.GetChild(0).gameObject.layer = LayerMask.NameToLayer("Boss_render");
        m_currentRenderBoss.transform.GetChild(1).gameObject.layer = LayerMask.NameToLayer("Boss_render");
        m_currentRenderBoss.transform.GetChild(2).gameObject.layer = LayerMask.NameToLayer("Boss_render");
        m_currentRenderBoss.AddComponent<Pan3dObject>();
        m_currentRenderBoss.GetComponent<BossCtrl>().enabled = false;

        for (int i = 0; i < DataBackup.Instance.GetEarnedStarsOnBossFight(_name); i++)
        {
            m_bossRendererStars[i].gameObject.SetActive(true);
        }
    }


    public void ToggleMenucreen(bool status)
    {
        m_menuScreen.SetActive(status);
        m_bossScreen.SetActive(!status);
        GameCtrlr.instance.m_bossRendererComponents.SetActive(!status);
    }

    public void ToggleBossListDisplay(bool status)
    {
        m_bossListDisplay.SetActive(status);
        Destroy(m_currentRenderBoss);
        m_bossRenderer.SetActive(!status);
    }
    public void ToggleBossRenderDisplay(bool status)
    {
        m_bossRenderer.SetActive(status);
        m_bossListDisplay.SetActive(!status);

    }
}
