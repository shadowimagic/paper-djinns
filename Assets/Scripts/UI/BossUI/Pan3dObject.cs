﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pan3dObject : MonoBehaviour
{
    protected Vector3 m_postLastFrame;
    public Camera m_UIcCam;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            m_postLastFrame = Input.mousePosition;

        if(Input.GetMouseButton(0))
        {
            var delta = Input.mousePosition - m_postLastFrame;
            m_postLastFrame = Input.mousePosition;

            var axis = Quaternion.AngleAxis(-90f, Vector3.forward) * delta;
            transform.rotation = Quaternion.AngleAxis(delta.magnitude * 0.1f, axis) * transform.rotation;
        }
    }
}
