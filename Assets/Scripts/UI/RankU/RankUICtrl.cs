﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankUICtrl : MonoBehaviour
{
    public GameObject m_menuScreen;

    public void ToggleMenucreen(bool status)
    {
        m_menuScreen.SetActive(status);
        gameObject.SetActive(!status);
    }
}
