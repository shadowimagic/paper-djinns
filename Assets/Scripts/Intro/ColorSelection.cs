﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSelection : MonoBehaviour
{

    float timeOfTravel = 5f; //time after object reach a target place 
    float currentTime = 0; // actual floting time 
    float normalizedValue;
    public List<GameObject> m_listOfColors;

    bool m_isScrolling;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ScrollUp()
    {
        if(!GetIsScrollingVal())
        {
            Vector3 _lastPosition = new Vector3();
            List<GameObject> _colors = new List<GameObject>();
            SetIsScrolliing(ref m_isScrolling, true);
            for (int i = m_listOfColors.Count - 1; i >= 0; i--)
            {
                if (i == m_listOfColors.Count - 1)
                {
                    _lastPosition = new Vector3(m_listOfColors[i].transform.position.x, m_listOfColors[i].transform.position.y, m_listOfColors[i].transform.position.z);
                }
                if (i != 0)
                {
                    Debug.Log(i);
                    StartCoroutine(LerpObject(m_listOfColors[i].transform, m_listOfColors[i].transform.position, m_listOfColors[i - 1].transform.position, i));
                    _colors.Add(m_listOfColors[i]);
                }
                else
                {
                    //lerp last position to the top
                    m_listOfColors[i].transform.position = new Vector3(_lastPosition.x, _lastPosition.y, _lastPosition.z);
                    _colors.Add(m_listOfColors[i]);
                    StartCoroutine(ResetIsScrollingVal());

                }
            }
            //_colors.Reverse();
            m_listOfColors = _colors;
            m_isScrolling = false;
        }

    }
    public void ScrollDown()
    {
        if(!GetIsScrollingVal())
        {
            Vector3 _firstPosition = new Vector3();
            List<GameObject> _colors = new List<GameObject>();
            SetIsScrolliing(ref m_isScrolling, true);
            for (int i = 0; i < m_listOfColors.Count; i++)
            {
                if (i == 0)
                {
                    _firstPosition = new Vector3(m_listOfColors[i].transform.position.x, m_listOfColors[i].transform.position.y, m_listOfColors[i].transform.position.z);
                    _colors.Add(m_listOfColors[m_listOfColors.Count - 1]);
                }
                if (i != m_listOfColors.Count - 1)
                {
                    StartCoroutine(LerpObject(m_listOfColors[i].transform, m_listOfColors[i].transform.position, m_listOfColors[i + 1].transform.position, i));
                    _colors.Add(m_listOfColors[i]);
                }
                else
                {
                    //lerp last position to the bottom
                    m_listOfColors[i].transform.position = new Vector3(_firstPosition.x, _firstPosition.y, _firstPosition.z);
                    StartCoroutine(ResetIsScrollingVal());
                }
            }
            m_listOfColors = _colors;
        }
    }


    IEnumerator ResetIsScrollingVal()
    {
        yield return new WaitForSeconds(1.5f);
        SetIsScrolliing(ref m_isScrolling, false);
    }

    public void SetIsScrolliing(ref bool _scrolling, bool _value)
    {
        _scrolling = _value;
    }

    public bool GetIsScrollingVal()
    {
        return m_isScrolling;
    }
    //IEnumerator LerpLastObject(Transform transform, Vector3 startPosition)
    //{

    //}


    IEnumerator LerpObject(Transform transform, Vector3 startPosition, Vector3 endPosition, int _colorIndex)
    {
        currentTime = 0f;
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 

            transform.position = Vector3.Lerp(startPosition, endPosition, normalizedValue);
            yield return null;
        }
    }
}


