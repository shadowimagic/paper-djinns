﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MyUtils;
public class IntroManager : MonoBehaviour
{
    public GameObject m_intro;
    public TMP_InputField m_inputField;
    int m_selectedColorIndex = 3;
    public ColorSelection m_colorSelection;
    public TextMeshProUGUI m_playerNameTxt;
    public Image m_playerColor;
    public PersistentInt m_hasIntroPlayed = new PersistentInt("INTRO_PLAYED", 0);

    private void Awake()
    {
        if(HasIntroPlayed())    //verified if the intro has been already played
        {
            gameObject.SetActive(false);    //Desactivated intro canvas
        }
    }
    public void CloseIntro()
    {
        if(!m_inputField.text.Equals(""))
        {
            Player.Instance.m_playerName.Set(m_inputField.text);
            Player.Instance.m_playerColor.Set(ColorUtility.ToHtmlStringRGB(m_colorSelection.m_listOfColors[m_selectedColorIndex].transform.GetChild(0).GetComponent<Image>().color));
            m_playerNameTxt.text = Player.Instance.m_playerName.Get();
            m_playerColor.color = m_colorSelection.m_listOfColors[m_selectedColorIndex].transform.GetChild(0).GetComponent<Image>().color;
            Debug.Log(ColorUtility.ToHtmlStringRGB(m_colorSelection.m_listOfColors[m_selectedColorIndex].transform.GetChild(0).GetComponent<Image>().color));
            Debug.Log(Player.Instance.m_playerName.Get());
            SetIntroToPlayed();
            m_intro.SetActive(false);
        }
    }


    public bool HasIntroPlayed()
    {
        if(m_hasIntroPlayed.Get() == 1) //verified if the intro has been played
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetIntroToPlayed()
    {
        m_hasIntroPlayed.Set(1);
    }



}
