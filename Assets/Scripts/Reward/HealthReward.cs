﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthReward : MonoBehaviour
{
    const int MAX_HEALTH = 6;
    const string HEALTH_TIMER = "HEALTH_REWARD_TIMER";
    public float msToWait;
    const float TOTAL_MS = 64800000;
    ulong timeStamp;
    public Image m_healthBar;
    float m_secondsLeft;
    public GameObject m_healthBarNotif;
    // Start is called before the first frame update
    void Start()
    {
        if(!PlayerPrefs.HasKey(HEALTH_TIMER))
        {
            PlayerPrefs.SetString(HEALTH_TIMER, DateTime.Now.Ticks.ToString());
        }
        timeStamp = ulong.Parse(PlayerPrefs.GetString(HEALTH_TIMER));
        IncreasedSlider();
    }

    // Update is called once per frame
    void Update()
    {
        CalculateHoursLeft();
    }

    public void CalculateHoursLeft()
    {

        ulong diff = (ulong)DateTime.Now.Ticks - timeStamp;
        ulong m = diff / TimeSpan.TicksPerMillisecond;
        m_secondsLeft = (float)(msToWait - m) / 1000.0f;
        string r = "";
        //hours
        r += ((int)m_secondsLeft / 3600).ToString() + "h ";
        m_secondsLeft -= ((int)m_secondsLeft / 3600) * 3600;
        //minutes
        r += ((int)m_secondsLeft / 60).ToString("00") + "m ";
        //seconds
        r += ((int)m_secondsLeft % 60).ToString("00") + "s ";
        IncreasedBar(m_secondsLeft);
    }

    public void IncreasedBar(float _secondsLeft)
    {

        if (_secondsLeft < 0)   //verified if the time is up
        {
            PlayerPrefs.SetString(HEALTH_TIMER, DateTime.Now.Ticks.ToString()); //reset the timer
            timeStamp = ulong.Parse(PlayerPrefs.GetString(HEALTH_TIMER));
            GameCtrlr.instance.m_currentHealth.Set(GameCtrlr.instance.m_currentHealth.Get() + 1);
            IncreasedSlider();
            GameCtrlr.instance.m_characterCtrlr.InitHealthBar();
        }
    }
    
    public void IncreasedSlider()
    {
        Debug.Log(GameCtrlr.instance.m_currentHealth.Get());
        float _percent = (float)GameCtrlr.instance.m_currentHealth.Get() / MAX_HEALTH;
        m_healthBar.fillAmount = _percent;
        if(GameCtrlr.instance.m_currentHealth.Get() > MAX_HEALTH)
        {
            int diff = GameCtrlr.instance.m_currentHealth.Get() - MAX_HEALTH;
            m_healthBarNotif.SetActive(true);
            m_healthBarNotif.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = diff.ToString();
        }
    }
}
