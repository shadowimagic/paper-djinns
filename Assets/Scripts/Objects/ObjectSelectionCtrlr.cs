﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyUtils;
using System.Linq;

public enum OBJECT_TYPE
{
    NONE,
    ROAD,
    STAIRWAY,
    CROSSROADS,
    TURNING,
    VILLAGE

}

public enum OBJECT_STATE
{
    SELECTED,
    UNSELECTED
}
public class ObjectSelectionCtrlr : Singleton<ObjectSelectionCtrlr>
{

    public OBJECT_TYPE m_selectedType = OBJECT_TYPE.NONE;  //selected the object type
    public List<GameObject> m_objectsBtn;   //list of objects
    public List<ObjectStruct> m_objects;    //decoration objects
    public GameObject m_selectedGrid;   //get the selected grid
    public GameObject m_gridContainer;  //grid container


    public void SelectObject(string _type)
    {
        OBJECT_TYPE _retirevedType = GetObjectTypeByString(_type);   //get the retrieved type
        if (m_selectedType == OBJECT_TYPE.NONE || _retirevedType != m_selectedType)
        {
            UnselectAllObjects();
            m_selectedType = _retirevedType;
            m_objects[(int)m_selectedType].btn.GetComponent<Image>().color = Color.green;
        }
        else
        {
            UnselectAllObjects();
            m_selectedType = OBJECT_TYPE.NONE;
        }
    }

    public void UnselectAllObjects()
    {
        int _index = 0;
        foreach(ObjectStruct c in m_objects)
        {
            if (c.type != OBJECT_TYPE.NONE)
            {
                Debug.Log("index " + _index);
                c.btn.GetComponent<Image>().color = Color.white;
            }
            _index++;
        }
    }

    public OBJECT_TYPE GetObjectTypeByString(string _type)
    {
        if(OBJECT_TYPE.NONE.ToString().Equals(_type))
        {
            return OBJECT_TYPE.NONE;
        }
        else if(OBJECT_TYPE.ROAD.ToString().Equals(_type))
        {
            return OBJECT_TYPE.ROAD;
        }
        else if(OBJECT_TYPE.STAIRWAY.ToString().Equals(_type))
        {
            return OBJECT_TYPE.STAIRWAY;
        }
        else if(OBJECT_TYPE.CROSSROADS.ToString().Equals(_type))
        {
            return OBJECT_TYPE.CROSSROADS;
        }
        else if(OBJECT_TYPE.TURNING.ToString().Equals(_type))
        {
            return OBJECT_TYPE.TURNING;
        }
        else if(OBJECT_TYPE.VILLAGE.ToString().Equals(_type))
        {
            return OBJECT_TYPE.VILLAGE;
        }
        else
        {
            return OBJECT_TYPE.NONE;
        }
    }

    public GameObject GetPrefabByType(OBJECT_TYPE _type)
    {
        return m_objects.Where(o => o.type == _type).FirstOrDefault().prefab;
    }

    public void RemovedPlacedObject()
    {
        if(m_selectedGrid != null && m_selectedGrid.transform.GetChild(0).transform.childCount > 0)
        {
            Destroy(m_selectedGrid.transform.GetChild(0).GetChild(0).gameObject);
            m_selectedGrid.GetComponent<GridParameter>().m_generatedObjectType = OBJECT_TYPE.NONE;
            m_selectedGrid = null;
        }
    }
    public void RotatePlacedObject()
    {
        if(m_selectedGrid != null && m_selectedGrid.transform.GetChild(0).transform.childCount > 0)
        {
                m_selectedGrid.transform.GetChild(0).GetChild(0).Rotate(0, m_selectedGrid.transform.GetChild(0).GetChild(0).rotation.y + 90f, 0);
        }
    }

    public void UnselectAllGrids()
    {
        foreach(Transform l in m_gridContainer.transform)
        {
            foreach(Transform gd in l.transform)
            {
                foreach(Transform g in gd.transform)
                {
                    g.GetComponent<GridParameter>().ResetMaterial();
                }
            }
        }
    }

    public void SnapToTheTop()
    {
        Vector3 _topPosition = m_selectedGrid.transform.Find("Up").position;
        m_selectedGrid.transform.GetChild(0).GetChild(0).transform.position = _topPosition;
    }


}

[System.Serializable]
public class ObjectStruct
{
    public OBJECT_TYPE type;
    public GameObject btn;
    public GameObject prefab;
}