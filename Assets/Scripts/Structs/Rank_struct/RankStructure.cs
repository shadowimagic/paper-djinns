﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class RankStruct
{
    public List<Rank> ranks;
}

[System.Serializable]
public class Rank
{
    public int landing;
    public string rank;
    public string imageName;
}
