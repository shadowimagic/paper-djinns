﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Audio;
using System;
using MyUtils;

public class AudioManager : Singleton<AudioManager>
{
    public List<Sound> sounds;


    const string c_defaultSoundtrack = "DEFAULT_SOUNDTRACK";
    const string c_bossSoundtrack = "BOSS_SOUNDTRACK";
    public GameObject m_camera;

    private void Awake()
    {

        foreach (Sound s in sounds)
        {
            s.source = gameObject.GetComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.pitch = s.pitch;
            s.source.volume = s.volume;
            s.source.loop = s.loop;
            s.source.name = s.clipName;
        }
    }

    private void Start()
    {
        StartCoroutine(fadeInDefaultSoundtrack());
    }



    /// <summary>
    /// Play a specific sound
    /// </summary>
    /// <param name="clipName">the string helps to find the corresponding sound to play</param>
    public void Play(string clipName)
    {
        Sound s = sounds.Where(so => so.clipName.ToLower().Equals(clipName.ToLower())).FirstOrDefault();
        s.source.PlayOneShot(s.clip);
    }

    /// <summary>
    /// Stop a specific sound
    /// </summary>
    /// <param name="clipName">the string helps to find the sound that need to be stopped</param>
    public void StopSound(string clipName)
    {
        Sound s = sounds.Where(so => so.clipName.ToLower().Equals(clipName.ToLower())).FirstOrDefault();

        if (s != null)
        {
            s.source.Stop();
        }
    }




     public IEnumerator fadeInDefaultSoundtrack()
    {
        fadeOutBossSoundtrack();
        Sound _s = sounds.Where(so => so.clipName.Equals(c_defaultSoundtrack)).FirstOrDefault();
        _s.source.volume = 0.0f;
        _s.source.clip = _s.clip;
        _s.source.PlayOneShot(_s.clip);
        _s.source.loop = true;
        float t = 0.0f;

        while (t < 0.1f)
        {
            t += Time.deltaTime;
            _s.source.volume = t;
            yield return new WaitForSeconds(0);
        }

    }
    public void fadeOutDefaultSoundtrack()
    {
        Sound _s = sounds.Where(so => so.clipName.Equals(c_defaultSoundtrack)).FirstOrDefault();
        _s.source.volume = 0.0f;
        _s.source.clip = _s.clip;
        _s.source.loop = true;
        float t = 0.1f;
        while (t > 0.0f)
        {
            t -= Time.deltaTime;
            _s.volume = t;
        }
        _s.source.Stop();

    }

    public IEnumerator fadeInBossSoundtrack()
    {
        fadeOutDefaultSoundtrack();
        Sound _s = sounds.Where(so => so.clipName.Equals(c_bossSoundtrack)).FirstOrDefault();
        _s.source.volume = 0.0f;
        _s.source.clip = _s.clip;
        _s.source.PlayOneShot(_s.clip);
        _s.source.loop = true;
        float t = 0.0f;

        while (t < 0.1f)
        {
            t += Time.deltaTime;
            _s.source.volume = t;
            yield return new WaitForSeconds(0);
        }

    }
    public void fadeOutBossSoundtrack()
    {
        Sound _s = sounds.Where(so => so.clipName.Equals(c_bossSoundtrack)).FirstOrDefault();
        _s.source.volume = 0.0f;
        _s.source.clip = _s.clip;
        _s.source.loop = true;
        float t = 0.1f;
        while (t > 0.0f)
        {
            t -= Time.deltaTime;
            _s.volume = t;
        }
        _s.source.Stop();

    }


}
