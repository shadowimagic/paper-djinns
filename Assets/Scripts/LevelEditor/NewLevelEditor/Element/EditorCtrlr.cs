﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyUtils;
using System.IO;
using UnityEngine.EventSystems;

public class EditorCtrlr : Singleton<EditorCtrlr>
{
    public GameObject m_selectedElementIndicator;    //show the current selected element
    public GameObject m_selectedElement;    //selected element
    public GameObject m_roadsContainer; //road container

    //List of available elements that could be generated 
    public List<ElementStruct> m_availableElements;
    //BUTTONS CONFIGS
    public GameObject m_buttonsContainer;   //contains the buttons which generate the clicked element
    public GameObject m_buttonPrefab;   //prefab used to generate the list of buttons
    public InputField m_levelIndexText; //get the current level index
    public LevelStruct m_levelStruct;   //level struct which contains the loaded roads
    public List<string> m_ignoredGameIbject;     //list of ignored gameObjects
    private const string ENEMY_OBJ_NAME = "RingPrefab";
    private const string ROAD_PROPS_NAME = "RoadProps";
    public GameObject m_enemyConfigs;   //enemy configs
    //ENEMY CONFIGS 
    public GameObject m_bossOptionsContainer;   //contains all the boss options
    public Toggle m_isEnemyActivatedToggle; //toggle which allows to activate enemy
    public Toggle m_isLastRoadToggle;   //Toggle which allows us to define the last road
    public Slider m_enemiesSpeedSlider; //slider which allows to set the enemies speed
    public Slider m_numberEnemiesSlider;    //slider which allows to set the number of enemy
    public InputField m_maxColorsInputField;    //max colors input field
    //BOSS CONFIGS
    public Toggle m_bossConfigsToggle;
    public InputField m_nbrOfBoxesInputField;   //nbr of boxes input field
    public InputField m_boxesColorsInputField;  //nbr of boxes input field
    public InputField m_bulletColorsInputField; //bullets colors
    //ROAD PROPS 
    public GameObject m_roadProps;  //contains the road props configs
    public Dropdown m_roadPropsDropdown;    //contains the road props dropdown elements
    private const string DESTRUCTIBLE_BOX_NAME = "DestructibleBox";
    private const string UNDESTRUCTIBLE_BOX_NAME = "UndestructibleBox";
    private const string OBSTACLE_NAME = "Obstacle";

    public void GenerateElement(string _elementName)
    {
        GameObject _elementPrefab = GetSelectedObjBasedOnName(_elementName);//Generate the gameObject of the clicked element
        Vector3 _pos = GetSelectedPosBasedOnName(_elementName);//get the position of the selected element based of the local position
        Quaternion _rot = GetSelectedRotBasedOnName(_elementName);//get the rotation of the selected element based of the local rotation
        GameObject _newElement = Instantiate(_elementPrefab, m_selectedElement.transform.parent.transform);
        _newElement.transform.position = _pos;
        _newElement.transform.rotation = _rot;

    }

    public Vector3 GetSelectedPosBasedOnName(string _elementName)
    {
        foreach(Transform t in m_selectedElement.transform)
        {
            if(t.gameObject.name.Equals(_elementName))
            {
                return transform.TransformPoint(t.position);
            }
        }
        return new Vector3();
    }

    public Quaternion GetSelectedRotBasedOnName(string _elementName)
    {
        foreach(Transform t in m_selectedElement.transform)
        {
            if(t.gameObject.name.Equals(_elementName))
            {
                return t.rotation;
            }
        }
        return new Quaternion();
    }

    public GameObject GetSelectedObjBasedOnName(string _elementName)
    {
        foreach(ElementStruct o in m_availableElements)
        {
            if(o.elementName.Equals(_elementName))
            {
                return o.elementPrefab;
            }
        }
        return null;
    }

    public void DeleteElement()
    {
        if(m_selectedElement != null)
        {
            Destroy(m_selectedElement);
        }
    }

    public void GenerateListOfElementsBtns(GameObject _element)
    {
        RemoveAllBtns();
        bool _isEnemyConfigsActivated = false;
        bool _isRoadPropsActivated = false;
        foreach(Transform t in _element.transform)  //get the childs of the current element and for each child we must generate a btn
        {
            if(!IsIgnoredGameObject(t.gameObject.name))
            {
                GameObject _btn = Instantiate(m_buttonPrefab, m_buttonsContainer.transform);
                _btn.GetComponent<Button>().onClick.AddListener(delegate { GenerateElement(t.gameObject.name); });  //set the method of the button
                _btn.transform.GetChild(0).GetComponent<Text>().text = t.gameObject.name;   //set the element name
            }
            if(t.gameObject.name.ToLower().Equals(ENEMY_OBJ_NAME.ToLower()))
            {
                _isEnemyConfigsActivated = true;
                m_enemyConfigs.SetActive(true); //activated the enemy configs when the enemy obj exist
                SetRoadEnemyConfigs();  //set the road enemy configs
            }
            _isRoadPropsActivated = VerifiedRoadProps(t.gameObject.name);
        }
        if(!_isEnemyConfigsActivated)
        {
            m_enemyConfigs.SetActive(false);
        }
        if(!_isRoadPropsActivated)
        {
            m_roadProps.SetActive(false);
        }
    }

    public bool VerifiedRoadProps(string _objName)
    {
        bool _isRoadPropsActivated = false;
        if(_objName.ToLower().Equals(ROAD_PROPS_NAME.ToLower()))
        {
            m_roadProps.SetActive(true);
            _isRoadPropsActivated = true;
        }
        return _isRoadPropsActivated;
    }

    public void ActivateRoadProps()
    {
        string _clickedBtnName = EventSystem.current.currentSelectedGameObject.name;    //get the clicked btn name
        if(m_roadPropsDropdown.options[m_roadPropsDropdown.value].text.ToLower().Equals(DESTRUCTIBLE_BOX_NAME.ToLower()))//verified the current value of the dropdown
        {
            ActivateCorrespondingRoadProps(DESTRUCTIBLE_BOX_NAME, _clickedBtnName, 90f);
        }
        else if (m_roadPropsDropdown.options[m_roadPropsDropdown.value].text.ToLower().Equals(UNDESTRUCTIBLE_BOX_NAME.ToLower()))
        {
            ActivateCorrespondingRoadProps(UNDESTRUCTIBLE_BOX_NAME, _clickedBtnName, 0f);
        }
        else if (m_roadPropsDropdown.options[m_roadPropsDropdown.value].text.ToLower().Equals(OBSTACLE_NAME.ToLower()))
        {
            ActivateCorrespondingRoadProps(OBSTACLE_NAME, _clickedBtnName, 0f);
        }
    }

    public void ActivateCorrespondingRoadProps(string _propsType, string _propsPos, float _angleOfRotation)
    {
        GameObject _roadProps = m_selectedElement.transform.Find("RoadProps").gameObject;
        if(_propsType.ToLower().Equals(UNDESTRUCTIBLE_BOX_NAME.ToLower()))
        {
            GameObject _box = _roadProps.transform.Find("Box").gameObject;
            ActivateRoadPropsBasedOnPos(_box, _propsPos, 90f);
        }
        if(_propsType.ToLower().Equals(DESTRUCTIBLE_BOX_NAME.ToLower()))
        {
            GameObject _box = _roadProps.transform.Find("Box").gameObject;
            ActivateRoadPropsBasedOnPos(_box, _propsPos, 0f);
        }
        else if(_propsType.ToLower().Equals(OBSTACLE_NAME.ToLower()))
        {
            GameObject _box = _roadProps.transform.Find("Obstacle").gameObject;
            ActivateRoadPropsBasedOnPos(_box, _propsPos, 0f);
        }
    }

    /// <summary>
    /// Activated the road props based on the selected positions
    /// </summary>
    /// <param name="_container"></param>
    /// <param name="_pos"></param>
    public void ActivateRoadPropsBasedOnPos(GameObject _container, string _pos, float _angle)
    {
        foreach(Transform t in _container.transform)
        {
            if(t.gameObject.name.ToLower().Equals(_pos.ToLower()))
            {
                t.gameObject.SetActive(!t.gameObject.activeInHierarchy);
                t.eulerAngles = new Vector3(t.eulerAngles.x, _angle, t.eulerAngles.z);
            }
        }
    }

    //toggle the boss configs
    public void ToggleBossConfigs()
    {
        m_bossOptionsContainer.SetActive(m_bossConfigsToggle.isOn);
    }

    public void SetRoadEnemyConfigs()
    {
        m_isEnemyActivatedToggle.isOn = m_selectedElement.GetComponent<ElementCtrlr>().m_isEnemyActivated;
        m_isLastRoadToggle.isOn = m_selectedElement.GetComponent<ElementCtrlr>().m_isLastRoad;
        m_enemiesSpeedSlider.value = m_selectedElement.GetComponent<ElementCtrlr>().m_enemySpeed;
        m_numberEnemiesSlider.value = m_selectedElement.GetComponent<ElementCtrlr>().m_numberOfEnemies;
    }

    public bool IsIgnoredGameObject(string _name)
    {
        foreach(string i in m_ignoredGameIbject)
        {
            if(i.ToLower().Equals(_name.ToLower()))
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveAllBtns()
    {
        for(int i = 0; i < m_buttonsContainer.transform.childCount; i++)
        {
            Destroy(m_buttonsContainer.transform.GetChild(i).gameObject);
        }
    }

    public List<LevelArtStruct> GetLevelArtsElements()
    {
        List<LevelArtStruct> _levelArtStruct = new List<LevelArtStruct>();
        foreach(Transform t in m_roadsContainer.transform)
        {
            if(!t.gameObject.name.Equals("SelectedObject"))
            {
                LevelArtStruct _art = new LevelArtStruct();
                _art.elementName = t.gameObject.name.Replace("(Clone)", "");
                _art.position = t.position;
                _art.rotation = t.rotation;
                _art.hasEnemies = t.GetComponent<ElementCtrlr>().m_isEnemyActivated;
                _art.isLastRoad = t.GetComponent<ElementCtrlr>().m_isLastRoad;
                _art.enemiesSpeed = t.GetComponent<ElementCtrlr>().m_enemySpeed;
                _art.numberOfEnemies = t.GetComponent<ElementCtrlr>().m_numberOfEnemies;
                _art.roadProps = GetRoadProps(t);
                _levelArtStruct.Add(_art);
            }
        }
        return _levelArtStruct;
    }

    public List<RoadPropsStruct> GetRoadProps(Transform _currentRoad)
    {
        List<RoadPropsStruct> _roadPropsStruct = new List<RoadPropsStruct>();
        foreach(Transform t in _currentRoad.transform)//verified if the current road has road props
        {
            if(t.transform.gameObject.name.ToLower().Equals("roadprops"))
            {
                foreach(Transform tc in t.transform.GetChild(0).transform)//verified if box item are activated
                {
                    if(tc.gameObject.activeInHierarchy) //verified if the GameObject is activated in the hierachy
                    {
                        RoadPropsStruct _r = new RoadPropsStruct();
                        if(tc.eulerAngles.y == 90f)
                        {
                            _r.roadPropsType = "DestructibleBox";
                        }
                        else
                        {
                            _r.roadPropsType = "UndestructibleBox";
                        }
                        _r.roadPropsPos = tc.gameObject.name;
                        _roadPropsStruct.Add(_r);   //added the new road props
                    }
                }
                foreach(Transform tc in t.transform.GetChild(1).transform)//verified if box item are activated
                {
                    if(tc.gameObject.activeInHierarchy) //verified if the GameObject is activated in the hierachy
                    {
                        RoadPropsStruct _r = new RoadPropsStruct();
                        _r.roadPropsType = "Obstacle";
                        _r.roadPropsPos = tc.gameObject.name;
                        _roadPropsStruct.Add(_r);   //added the new road props
                    }
                }
            }
        }
        return _roadPropsStruct;
    }

    public void SaveLevel()
    {
        LevelStruct _levelStruct = new LevelStruct();
        _levelStruct.levelArtsStruct = GetLevelArtsElements();
        _levelStruct.bossStruct = new BossStruct();
        _levelStruct.bossStruct.nbrBoxes = int.Parse(m_nbrOfBoxesInputField.text);
        _levelStruct.bossStruct.nbrOfBoxesColors = int.Parse(m_boxesColorsInputField.text);
        _levelStruct.bossStruct.nbrOfBulletColors = int.Parse(m_bulletColorsInputField.text);
        _levelStruct.m_maxColors = int.Parse(m_maxColorsInputField.text);
        string levelArtJSON = JsonUtility.ToJson(_levelStruct);
        //writing the data to the file path
        string path = "/Resources/Levels/Level_" + m_levelIndexText.text + ".json";
        WriteDataToFile(levelArtJSON, path);
    }

    public void LoadLevel()
    {
        string path = "Levels/Level_" + m_levelIndexText.text;
        TextAsset json = Resources.Load(path) as TextAsset;
        DestroyCurrentRoadsObj();
        if (json != null)
        {
            m_levelStruct = new LevelStruct();
            m_levelStruct.levelArtsStruct = new List<LevelArtStruct>();
            m_levelStruct = JsonUtility.FromJson<LevelStruct>(json.text);
            //generate the list of roads into the scene
            GenerateRoads();
        }
        else
        {
            Debug.Log("CAN T FIND " + json.text);
        }
    }


    public void DestroyCurrentRoadsObj()
    {
        for(int i = 0; i < m_roadsContainer.transform.childCount; i++ )
        {
            Destroy(m_roadsContainer.transform.GetChild(i).gameObject); //destroyed the road child 
        }
    }

    public void GenerateRoads()
    {
        foreach(LevelArtStruct l in m_levelStruct.levelArtsStruct)
        {
            GameObject _elementPrefab = GetSelectedObjBasedOnName(l.elementName);//Generate the gameObject of the clicked element
            GameObject _road = Instantiate(_elementPrefab, m_roadsContainer.transform);
            _road.transform.position = l.position;
            _road.transform.rotation = l.rotation;
            GenerateRoadProps(_road, l);
        }
    }


    public void GenerateRoadProps(GameObject _road, LevelArtStruct _levelArt)
    {
        if (_road.transform.Find("RoadProps") != null)
        {
            GameObject _roadProps = _road.transform.Find("RoadProps").gameObject;
            foreach (RoadPropsStruct r in _levelArt.roadProps)//verified if the current road has road props
            {
                if (r.roadPropsType.Equals("Box"))
                {
                    Debug.Log("Found");
                    foreach (Transform tc in _roadProps.transform.GetChild(0).transform)//verified if box item are activated
                    {
                        if (tc.gameObject.name.ToLower().Equals(r.roadPropsPos.ToLower())) //verified if the GameObject is activated in the hierachy
                        {
                            tc.gameObject.SetActive(true);
                            tc.GetComponent<RoadPropCtrlr>().m_roadPropType = "Box";
                        }
                    }
                }
                if (r.roadPropsType.Equals("Obstacle"))
                {
                    foreach (Transform tc in _roadProps.transform.GetChild(1).transform)//verified if box item are activated
                    {
                        if (tc.gameObject.name.ToLower().Equals(r.roadPropsPos.ToLower())) //verified if the GameObject is activated in the hierachy
                        {
                            tc.GetComponent<RoadPropCtrlr>().m_roadPropType = "Obstacle";
                            tc.gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
    }

    public void ToggleEnemyBool(GameObject _toggle)
    {
        m_selectedElement.GetComponent<ElementCtrlr>().m_isEnemyActivated = _toggle.GetComponent<Toggle>().isOn;
    }

    public void ToggleIsLastRoadBool(GameObject _toggle)
    {
        m_selectedElement.GetComponent<ElementCtrlr>().m_isLastRoad = _toggle.GetComponent<Toggle>().isOn;
    }

    public void AssignNumberOfEnemies(GameObject _slider)
    {
        m_selectedElement.GetComponent<ElementCtrlr>().m_numberOfEnemies = (int)_slider.GetComponent<Slider>().value;
    }
    public void AssignEnemiesSpeed(GameObject _slider)
    {
        m_selectedElement.GetComponent<ElementCtrlr>().m_enemySpeed = _slider.GetComponent<Slider>().value;
    }

    public static void WriteDataToFile(string jsonString, string filePath)
    {
        string path = Application.dataPath + filePath;
        Debug.Log("AssetPath:" + path);
        File.WriteAllText(path, jsonString);
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }
}

[System.Serializable]
public class ElementStruct
{
    public string elementName;
    public GameObject elementPrefab;
}

[System.Serializable]
public class LevelStruct
{
    public int m_maxColors;
    public List<LevelArtStruct> levelArtsStruct;
    public BossStruct bossStruct;
}

[System.Serializable]
public class LevelArtStruct
{
    public string elementName;
    public Vector3 position;
    public Quaternion rotation;
    public bool hasEnemies;
    public int numberOfEnemies;
    public float enemiesSpeed;
    public bool isLastRoad;
    public List<RoadPropsStruct> roadProps;
}

[System.Serializable]
public class RoadPropsStruct
{
    public string roadPropsType;
    public string roadPropsPos;
}

[System.Serializable]
public class BossStruct
{
    public int nbrBoxes;
    public int nbrOfBoxesColors;
    public int nbrOfBulletColors;
}