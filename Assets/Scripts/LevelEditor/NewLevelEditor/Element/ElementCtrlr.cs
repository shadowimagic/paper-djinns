﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementCtrlr : MonoBehaviour
{
    public bool m_isEnemyActivated;
    public int m_numberOfEnemies;
    public float m_enemySpeed;
    public bool m_isLastRoad;
    // Start is called before the first frame update
    void Start()
    {
        m_numberOfEnemies = 1;
        m_enemySpeed = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        EditorCtrlr.Instance.m_selectedElementIndicator.transform.position = new Vector3(transform.position.x, 49f, transform.position.z); //position the indicator at the current selected element
        EditorCtrlr.Instance.m_selectedElement = this.gameObject;   //assign the current selected element
        EditorCtrlr.Instance.GenerateListOfElementsBtns(gameObject);//show the available options 

    }
}
