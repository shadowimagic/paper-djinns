﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class SceneCreator : MonoBehaviour
{
    public List<GameObject> m_grids;
    public GameObject m_gridComponentContainer;
    string TOP_GRID_MAT_NAME = "TopGridMaterial (Instance)";
    string MID_GRID_MAT_NAME = "MidGridMaterial (Instance)";
    string LOW_GRID_MAT_NAME = "BottomGridMaterial (Instance)";
    public InputField m_levelIndexText; //get the current level index

    // Start is called before the first frame update
    void Start()
    {
        
    }


    /// <summary>
    /// Get the list of grid in the 
    /// </summary>
    /// <returns></returns>
    public List<GameObject> GetListOfGrids()
    {
        List<GameObject> _grids = new List<GameObject>();
        foreach(Transform t in m_gridComponentContainer.transform)
        {
            foreach(Transform _to in t.transform)
            {
                foreach(Transform _g in _to.transform)
                {
                    _grids.Add(_g.gameObject);
                }
            }
        }
        return _grids;
    }

    public int GetXGridPosition(GameObject _grid)
    {
        List<GameObject> _orderedGrid = m_grids.OrderBy(g => g.transform.position.x).ToList();
        int _index = 0;
        foreach(GameObject o in _orderedGrid)
        {
            if(Mathf.Abs(_grid.transform.position.x - o.transform.position.x) <= 1f)
            {
                return _index;
            }
            _index++;
        }
        return -1;
    }

    public int GetZGridPosition(GameObject _grid)
    {
        List<GameObject> _orderedGrid = m_grids.OrderBy(g => g.transform.position.z).ToList();
        int _index = 0;
        foreach(GameObject o in _orderedGrid)
        {
            if(Mathf.Abs(_grid.transform.position.z - o.transform.position.z) <= 1f)
            {
                return _index;
            }
            _index++;
        }
        return -1;
    }


    //public LevelArtStruct GetLevelArtStruct(List<GameObject> _grids)
    //{
    //    List<ArtStruct> _artObjs = new List<ArtStruct>();
    //    foreach (GameObject g in _grids)
    //    {
    //        if (g.transform.GetChild(0).transform.childCount > 0)
    //        {
    //            ArtStruct a = new ArtStruct();
    //            a.posxindex = GetXGridPosition(g);
    //            a.poszindex = GetXGridPosition(g);
    //            a.heightLevel = GetHeightLevel(g.GetComponent<MeshRenderer>().material.name);
    //            a.objectType = g.GetComponent<GridParameter>().m_generatedObjectType.ToString();
    //            a.gridposition = g.GetComponent<GridParameter>().m_gridPosition;
    //            _artObjs.Add(a);
    //        }
    //    }
    //    LevelArtStruct _levelArtStruct = new LevelArtStruct();
    //    _levelArtStruct.m_artObjs = new List<ArtStruct>();
    //    _levelArtStruct.m_artObjs = _artObjs;
    //    return _levelArtStruct;
    //}

    //public void SaveLevelArt()
    //{
    //    m_grids = GetListOfGrids();
    //    LevelArtStruct levelArtStruct = GetLevelArtStruct(m_grids);
    //    string levelArtJSON = JsonUtility.ToJson(levelArtStruct);
    //    //writing the data to the file path
    //    string path = "/Resources/Levels/Level_" + m_levelIndexText.text + ".json";
    //    WriteDataToFile(levelArtJSON, path);
    //}


    //public void LoadSeaDecorationJSON(string world, string level)
    //{
    //    string path = "CMB_WT/Levels_data/" + world + level + "/Sea_decoration";
    //    TextAsset json = Resources.Load(path) as TextAsset;
    //    DestroyObjsInScene(m_seaObjects);
    //    if (json != null)
    //    {
    //        m_listOfLoadedSeaDecoration = JsonUtility.FromJson<SeaObjs>(json.text);
    //        //generate the list of bins into the scene
    //        GeenerateSeaObjectsToScene(m_listOfLoadedSeaDecoration.seaObjects);
    //    }
    //    else
    //    {
    //        Debug.Log("CAN T FIND " + json.text);
    //    }

    //}

    public static void WriteDataToFile(string jsonString, string filePath)
    {
        string path = Application.dataPath + filePath;
        Debug.Log("AssetPath:" + path);
        File.WriteAllText(path, jsonString);
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }


    public string GetHeightLevel(string _materialName)
    {
        if(_materialName.Equals(TOP_GRID_MAT_NAME))
        {
            return "TOP";
        }
        else if(_materialName.Equals(MID_GRID_MAT_NAME))
        {
            return "MID";
        }
        else
        {
            return "BOTTOM";
        }
    }

}


//[System.Serializable]
//public class LevelArtStruct
//{
//    public List<ArtStruct> m_artObjs;
//}

//[System.Serializable]
//public class ArtStruct
//{
//    public int posxindex;    //get the element x position
//    public int poszindex;    //get the element z position
//    public string gridposition; //grid position
//    public string objectType; //get the object type 
//    public string  heightLevel;  
//}
