﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum GRID_POSITION
{
    FOWARD,
    LEFT,
    RIGHT
}

public enum GRID_LEVEL
{
    BOTTOM_LEVEL,
    MID_LEVEL,
    TOP_LEVEL
}
public class GridConfigs : MonoBehaviour
{
    public int m_numberOfGrid;
    public TMP_InputField m_nbrOfGridTxt;
    public TMP_Dropdown m_positionDropdown;
    public GameObject m_gridComponentPrefab;
    public float m_gridFowardOffset;
    public float m_gridLeftOffset;
    public float m_gridRightOffset;
    //CONTAINER
    public GameObject m_gridContainer;  //the grid container contains the different layers
    public List<Material> m_gridMaterial;
    public List<Toggle> m_gridLayersToggle;
    //GRID CONFIGS COMPONENTS
    public Toggle m_gridConfigToggle;
    public GameObject m_gridComponents;
    



    public void GenerateGrid()
    {
        for(int i = 0; i < int.Parse(m_nbrOfGridTxt.text); i++)
        {
            switch (m_positionDropdown.value)
            {
                case (int)GRID_POSITION.FOWARD:
                    GameObject _lastGridFoward = GetGridByPosition(GRID_POSITION.FOWARD);//should get the grid that is completely foward
                    GenerateGridsOnGrids(_lastGridFoward, GRID_POSITION.FOWARD);
                    break;
                case (int)GRID_POSITION.LEFT:
                    GameObject _lastGridLeft = GetGridByPosition(GRID_POSITION.LEFT);//should get the grid that  is completely on the left
                    GenerateGridsOnGrids(_lastGridLeft, GRID_POSITION.LEFT);
                    break;
                case (int)GRID_POSITION.RIGHT:
                    GameObject _lastGridRight = GetGridByPosition(GRID_POSITION.RIGHT);//should get the grid that  is completely on the right
                    GenerateGridsOnGrids(_lastGridRight, GRID_POSITION.RIGHT);
                    break;
            }
        }
    }

    public GameObject GetGridByPosition(GRID_POSITION _position)
    {
        GameObject _retrievedGrid = null;
        float _pos = 0f;
        int _index = 0;
        if(_position == GRID_POSITION.FOWARD)
        {
            foreach(Transform t in m_gridContainer.transform.GetChild(0).transform)
            {
                if(_index == 0)
                {
                    _pos = t.transform.position.z;
                    _retrievedGrid = t.gameObject;
                }
                else if(t.transform.position.z > _pos)
                {
                    _pos = t.transform.position.z;
                    _retrievedGrid = t.gameObject;
                }

                _index++;
            }
        }
        else if(_position == GRID_POSITION.LEFT)
        {
            foreach (Transform t in m_gridContainer.transform.GetChild(0).transform)
            {
                if (_index == 0)
                {
                    _pos = t.transform.position.z;
                    _retrievedGrid = t.gameObject;
                }
                else if (t.transform.position.x < _pos)
                {
                    _pos = t.transform.position.x;
                    _retrievedGrid = t.gameObject;
                }

                _index++;
            }
        }
        else
        {
            foreach (Transform t in m_gridContainer.transform.GetChild(0).transform)
            {
                if (_index == 0)
                {
                    _pos = t.transform.position.x;
                    _retrievedGrid = t.gameObject;
                }
                else if (t.transform.position.x > _pos)
                {
                    _pos = t.transform.position.x;
                    _retrievedGrid = t.gameObject;
                }

                _index++;
            }
        }

        return _retrievedGrid;
    }

    public void GenerateGridsOnGrids(GameObject _gridRef, GRID_POSITION _position)
    {
        int _size = 0;
        int _counter = 0;
        int _gridLevelIndex = 0;
        foreach(Transform t in m_gridContainer.transform)
        {
            _size = t.transform.childCount;
            _counter = 0;
            foreach(Transform tc in t.transform)
            {

                if (IsOnExtremity(_gridRef, tc.gameObject, _position))
                {
                    if (_position == GRID_POSITION.FOWARD)
                    {
                        GameObject _foward = Instantiate(m_gridComponentPrefab, tc.parent);//Generate a grid by using the last grid position plus offset
                        _foward.transform.position = new Vector3(tc.position.x, tc.position.y, tc.position.z + m_gridFowardOffset);    //set the generated foward grid position
                        SetGridComponentMaterial(_foward, _gridLevelIndex);

                    }
                    else if (_position == GRID_POSITION.LEFT)
                    {
                        GameObject _left = Instantiate(m_gridComponentPrefab, tc.parent);//Generate a grid by using the last grid position plus offset
                        _left.transform.position = new Vector3(tc.position.x + m_gridLeftOffset, tc.position.y, tc.position.z);    //set the generated foward grid position
                        SetGridComponentMaterial(_left, _gridLevelIndex);
                    }
                    else
                    {
                        GameObject _right = Instantiate(m_gridComponentPrefab, tc.parent);//Generate a grid by using the last grid position plus offset
                        _right.transform.position = new Vector3(tc.position.x + m_gridRightOffset, tc.position.y, tc.position.z);    //set the generated foward grid position
                        SetGridComponentMaterial(_right, _gridLevelIndex);
                    }
                }
                _counter++;
                if(_counter == _size)
                {
                    break;
                }
            }
            _gridLevelIndex++;
        }
    }

    public void SetGridComponentMaterial(GameObject _gridComponent, int _index)
    {
        foreach(Transform t in _gridComponent.transform)
        {
            t.GetComponent<MeshRenderer>().material = m_gridMaterial[_index];
        }
    }

    public bool IsOnExtremity(GameObject _gridRef, GameObject _currentGrid, GRID_POSITION _POSITION)
    {
        if(_POSITION == GRID_POSITION.FOWARD)
        {
            if((Mathf.Abs(_gridRef.transform.position.z) - Mathf.Abs(_currentGrid.transform.position.z)) < 10f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }else if(_POSITION == GRID_POSITION.LEFT || _POSITION == GRID_POSITION.RIGHT)
        {
            if ((Mathf.Abs(_gridRef.transform.position.x) - Mathf.Abs(_currentGrid.transform.position.x)) < 10f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void ToggleTopLayer()
    {
        if(m_gridLayersToggle[(int)GRID_LEVEL.TOP_LEVEL].isOn)
        {
            m_gridContainer.transform.GetChild((int)GRID_LEVEL.TOP_LEVEL).gameObject.SetActive(true);
        }
        else
        {
            m_gridContainer.transform.GetChild((int)GRID_LEVEL.TOP_LEVEL).gameObject.SetActive(false);
        }
    }

    public void ToggleMidLayer()
    {
        if(m_gridLayersToggle[(int)GRID_LEVEL.MID_LEVEL].isOn)
        {
            m_gridContainer.transform.GetChild((int)GRID_LEVEL.MID_LEVEL).gameObject.SetActive(true);
        }
        else
        {
            m_gridContainer.transform.GetChild((int)GRID_LEVEL.MID_LEVEL).gameObject.SetActive(false);
        }
    }

    public void ToggleBottomLayer()
    {
        if(m_gridLayersToggle[(int)GRID_LEVEL.BOTTOM_LEVEL].isOn)
        {
            m_gridContainer.transform.GetChild((int)GRID_LEVEL.BOTTOM_LEVEL).gameObject.SetActive(true);
        }
        else
        {
            m_gridContainer.transform.GetChild((int)GRID_LEVEL.BOTTOM_LEVEL).gameObject.SetActive(false);
        }
    }

    public void ToggleGridConfigsComponents()
    {
        if(m_gridConfigToggle.isOn)
        {
            m_gridComponents.SetActive(true);
        }
        else
        {
            m_gridComponents.SetActive(false);
        }
    }

}
