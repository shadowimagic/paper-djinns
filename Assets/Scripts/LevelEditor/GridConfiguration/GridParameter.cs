﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridParameter : MonoBehaviour
{
    public Material m_defaultMaterial;
    public Material m_hoverMaterial;
    public Material m_selectedMaterial;
    const string m_selectedMatName = "selectedMaterial (Instance)";
    public string m_gridPosition;
    public OBJECT_TYPE m_generatedObjectType;

    private void Start()
    {
            m_defaultMaterial = this.GetComponent<MeshRenderer>().material;
        Debug.Log(this.GetComponent<MeshRenderer>().material.name);
    }

    void OnMouseOver()
    {
        if (m_defaultMaterial == null)
        {
            m_defaultMaterial = this.GetComponent<MeshRenderer>().material;

        }
        if (!this.GetComponent<MeshRenderer>().material.name.Equals(m_selectedMatName))
        {
            this.GetComponent<MeshRenderer>().material = m_hoverMaterial;
        }
    }

    void OnMouseExit()
    {
        if (!this.GetComponent<MeshRenderer>().material.name.Equals(m_selectedMatName))

        {
            this.GetComponent<MeshRenderer>().material = m_defaultMaterial;
        }
    }

    private void OnMouseDown()
    {

        if (!this.GetComponent<MeshRenderer>().material.name.Equals(m_selectedMatName) && ObjectSelectionCtrlr.Instance.m_selectedType == OBJECT_TYPE.NONE)
        {
            ObjectSelectionCtrlr.Instance.UnselectAllGrids();
            this.GetComponent<MeshRenderer>().material = m_selectedMaterial;
            ObjectSelectionCtrlr.Instance.m_selectedGrid = this.gameObject; //set the selected grid
        }
        else
        {
            this.GetComponent<MeshRenderer>().material = m_defaultMaterial;
            ObjectSelectionCtrlr.Instance.m_selectedGrid = null;    //removed the current selected grid
        }
        if (ObjectSelectionCtrlr.Instance.m_selectedType != OBJECT_TYPE.NONE && transform.GetChild(0).childCount == 0)//verified if there is a selected type
        {
            GameObject _prefab = ObjectSelectionCtrlr.Instance.GetPrefabByType(ObjectSelectionCtrlr.Instance.m_selectedType);
            GameObject _o = Instantiate(_prefab);
            _o.transform.position = new Vector3(transform.position.x, transform.position.y + 50f, transform.position.z);
            _o.transform.parent = transform.GetChild(0);
            ObjectSelectionCtrlr.Instance.UnselectAllGrids();
            m_generatedObjectType = ObjectSelectionCtrlr.Instance.m_selectedType;
            //_o.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
    }

    public void ResetMaterial()
    {
        this.GetComponent<MeshRenderer>().material = m_defaultMaterial;
    }

}
