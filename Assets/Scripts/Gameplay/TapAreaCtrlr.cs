﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class TapAreaCtrlr : MonoBehaviour, IPointerClickHandler
{
    public GameObject m_helpUI;

    public AudioClip m_colorSwitchSound;
    
    public Button m_changeColorBtn;
    public Button m_shootBulletsBtn;

    // Start is called before the first frame update
    void Start()
    {
        m_changeColorBtn.interactable = false;
        m_shootBulletsBtn.interactable = false;

    }
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if(m_helpUI.activeInHierarchy)
        {
            m_helpUI.SetActive(false);
            GameCtrlr.instance.Go();
            FindObjectOfType<DecorsCtrlr>().StartAnimations();
            m_changeColorBtn.interactable = true;
            m_shootBulletsBtn.interactable = true;
        }
    }

    public void ChangeBulletsColor()
    {
        GameCtrlr.instance.m_audioSource.PlayOneShot(m_colorSwitchSound);
        GameCtrlr.instance.ChangeBulletColor();

    }

    public void ChangeBtnColor(Color c)
    {
        m_changeColorBtn.transform.GetChild(0).GetComponent<Image>().color = c;
    }

    public void ShootBullets()
    {

    }

}
