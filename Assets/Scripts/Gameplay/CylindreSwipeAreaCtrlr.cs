﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylindreSwipeAreaCtrlr : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    void OnTriggerExit(Collider collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(!GameCtrlr.instance.m_isCharacterBlinking)
            {
                transform.parent.GetComponent<CylindreCtrlr>().DestroyRoad();
            }
        }
    }

}
