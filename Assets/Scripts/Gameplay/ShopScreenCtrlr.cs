﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ShopScreenCtrlr : MonoBehaviour
{

    // MOST PART OF THIS SCRIPT LOGIC IS HARD CODED  . Running out of time ;)
    public TextMeshProUGUI m_levelText;
    public TextMeshProUGUI m_coinText;
    public TextMeshProUGUI m_gemText;

    public GameObject m_itemsContainer;

    public Button m_buyBtn;
    int m_currentIntemIndex = 0;

    int m_hardCodedCoins = 6000;
    int m_hardCodedGems = 1100;
    Color m_boughtColor = new Color(0.9411765f, 8000001f, 0.05882353f);
    

    // Start is called before the first frame update
    void Start()
    {

        int playerLevel = PlayerPrefs.GetInt("currentLevel");
        int playerCoins = PlayerPrefs.GetInt("coins");
        int playerGems = PlayerPrefs.GetInt("gems");
        
        m_levelText.text = playerLevel.ToString();
        m_coinText.text = playerCoins.ToString();
        m_gemText.text = playerGems.ToString();

        // Debug.Log("On");
        if(playerCoins >= m_hardCodedCoins && playerGems >= m_hardCodedGems)
        {
            if(PlayerPrefs.GetInt("isBought", 0) == 0)
            {
                m_buyBtn.interactable = true;
                Debug.Log("On");
            }
            else
            {
                Debug.Log("Off");
                m_itemsContainer.transform.GetChild(0).Find("Background").GetComponent<Image>().color = m_boughtColor;
                m_buyBtn.interactable = false;
            }
        }
        else
        {
            m_buyBtn.interactable = false;
        }

    }

    public void SlideDir(int dir)
    {
        int nextIndex = m_currentIntemIndex + dir;
        if(nextIndex < 0) nextIndex = 2; // 3 Items Max
        if(nextIndex >= 3 ) nextIndex = 0;

        m_itemsContainer.transform.GetChild(m_currentIntemIndex).gameObject.SetActive(false);
        m_itemsContainer.transform.GetChild(nextIndex).gameObject.SetActive(true);
        m_currentIntemIndex = nextIndex;

        if(nextIndex == 0)
        {
            // Hard Coded ;)
            int playerCoins = PlayerPrefs.GetInt("coins");
            int playerGems = PlayerPrefs.GetInt("gems");

            if(playerCoins >= m_hardCodedCoins && playerGems >= m_hardCodedGems)
            {
                if(PlayerPrefs.GetInt("isBought", 0) == 0)
                {
                    m_buyBtn.interactable = true;
                }
                else
                {
                    m_itemsContainer.transform.GetChild(0).Find("Background").GetComponent<Image>().color = m_boughtColor;
                    m_buyBtn.interactable = false;
                }
            }

        }
        else
        {
            m_buyBtn.interactable = false;
        }
    }

    public void Buy()
    {
        // Hard Coded ;)
        int playerCoins = PlayerPrefs.GetInt("coins");
        int playerGems = PlayerPrefs.GetInt("gems");

        playerCoins -= m_hardCodedCoins;
        playerGems -= m_hardCodedGems;

        PlayerPrefs.SetInt("coins", playerCoins);
        PlayerPrefs.GetInt("gems", playerGems);

        m_coinText.text = playerCoins.ToString();
        m_gemText.text = playerGems.ToString();

     
        GameCtrlr.instance.SetGunType(2);

        m_itemsContainer.transform.GetChild(0).Find("Background").GetComponent<Image>().color = m_boughtColor;

        PlayerPrefs.SetInt("isBought", 1);
        m_buyBtn.interactable = false;


    }

    public void SetupUI()
    {
        // Hard Coded ;)
        int playerCoins = PlayerPrefs.GetInt("coins");
        int playerGems = PlayerPrefs.GetInt("gems");

        if(playerCoins >= m_hardCodedCoins && playerGems >= m_hardCodedGems)
        {
            if(PlayerPrefs.GetInt("isBought", 0) == 0)
            {
                m_buyBtn.interactable = true;
            }
            else
            {
                m_itemsContainer.transform.GetChild(0).Find("Background").GetComponent<Image>().color = m_boughtColor;
                m_buyBtn.interactable = false;
            }
        }
    }

    public void GotoHome()
    {
        GameCtrlr.instance.FromShopToMenu();
    }

     
}

[System.Serializable]
public class Shop_item
{
    public bool isBought;

}

