﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerationCtrlr : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform m_mainCylindre;
    public GameObject m_roadSectionPrefab;
    float m_roadLength;

    int m_roadsCount = 2; // Starting with 2 chunks of Road
    void Start()
    {
        m_roadLength = m_mainCylindre.GetComponent<MeshRenderer>().bounds.size.z;
    }
    
    public void GenerateRoad()
    {
        GameObject instantiatedPrefab = Instantiate(m_roadSectionPrefab, Vector3.zero, m_roadSectionPrefab.transform.rotation, transform);
        float posZ = (m_roadLength * m_roadsCount) - 50.0f;
        instantiatedPrefab.transform.localPosition = new Vector3(0,0,posZ);
        m_roadsCount++;

    }
}
