﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingSectionCtrlr : MonoBehaviour
{
    // Start is called before the first frame update

    List<int> m_obstaclesIndexes = new List<int>();
    
    float m_rotRatio = 25f;
    float m_rotSpeed = 1;

    void Start()
    {
        m_rotSpeed = GameCtrlr.instance.m_cylindreRotSpeed;        
    }

    void Populate()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            m_obstaclesIndexes.Add(i);

            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

   public void DefineObstacles(int _numberOfObstacles)
   {
        // // Populating obstacles indexes and Hiding Obstacles
     Populate();

     for (int j = 0; j < _numberOfObstacles; j++)
     {
          int randomObstaclesIndexesIndex = Random.Range(0, m_obstaclesIndexes.Count);
          int randomObstacleIndex = m_obstaclesIndexes[randomObstaclesIndexesIndex];

          // Define Obstacle
          if(!GameCtrlr.instance.m_isBossActivated) //shouldn't generate enemies if the boss is activated
          {
                DefineObstacle(randomObstacleIndex, randomObstaclesIndexesIndex);
            }
        }
   }

   public void DefineObstacle(int _index, int _indexOfIndex)
   {
        transform.GetChild(_index).gameObject.SetActive(true);
        m_obstaclesIndexes.RemoveAt(_indexOfIndex);
        // Setting a material
        SetObstacleMaterial(transform.GetChild(_index));
   }

   void SetObstacleMaterial(Transform _obstacle)
   {
        int randomObstacleMaterialIndex = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));

        _obstacle.GetComponentInChildren<MeshRenderer>().sharedMaterial = GameCtrlr.instance.m_pickableObstaclesMaterials[randomObstacleMaterialIndex];
        string obstacleColorTag = GameCtrlr.instance.m_pickableObstaclesMaterials[randomObstacleMaterialIndex].name;
        string[] splitArray =  obstacleColorTag.Split(char.Parse("_")); //
        obstacleColorTag = splitArray[1];
        _obstacle.gameObject.tag = obstacleColorTag;
        _obstacle.GetChild(0).gameObject.tag = obstacleColorTag;
        _obstacle.GetChild(0).GetComponent<ObstacleCtrlr>().SetTag(obstacleColorTag);

   }
}
