﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCtrlr : MonoBehaviour
{
    public int m_point = 100;
    public int m_type = 1; // 1 for Coin, 2 for Gem , 3 for Heart, 4 For Star
    // Start is called before the first frame update

    public AudioClip m_coinSound;
    public AudioClip m_diamondSound;
    public AudioClip m_lifeSound;
    void Start()
    {
        
    }
    
    void OnTriggerEnter(Collider collision)
    {

        // If Bullet entered
        if(collision.gameObject.tag == "Player")
        {
            
            if(GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
            {
                switch (m_type)
                {
                    case 1 :
                        GameCtrlr.instance.m_audioSource.PlayOneShot(m_coinSound); 
                    break;
                    case 2 :
                        GameCtrlr.instance.m_audioSource.PlayOneShot(m_diamondSound); 
                    break;
                    case 3 :
                    case 4 :
                        GameCtrlr.instance.m_audioSource.PlayOneShot(m_lifeSound); 
                    break;
                    default:break;
                }
            }
                
                GetCoin(m_type, m_point);
                transform.GetComponent<BoxCollider>().enabled = false;
                //Destroy(transform.gameObject);
                // Debug.Log("Touched by Bro !");
        }
 
         
    }

    void GetCoin(int type, int point)
    {
        GameCtrlr.instance.GetCoin(type, point);
        transform.gameObject.SetActive(false);
    }
}
