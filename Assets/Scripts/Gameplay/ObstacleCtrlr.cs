﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCtrlr : MonoBehaviour
{
    string m_tag;

    public GameObject m_coinPrefab;
 
    public AudioClip m_explodingSound;
    public AudioClip m_loseLifeSound;

    public ParticleSystem m_particle;
    Material m_mat;
    public Material m_particleMat;


    // Start is called before the first frame update

    private void Awake() {
        
        // m_particle = transform.GetComponentInChildren<ParticleSystem>();
        m_mat = transform.GetComponent<MeshRenderer>().material;

    }
    void Start()
    {
        m_mat = transform.GetComponent<MeshRenderer>().material;

        m_particle.gameObject.SetActive(true);

        if(!GameCtrlr.instance.m_isStarPowerupOn)
        {
            SetTag(transform.parent.gameObject.tag);
        }
        else // Powerup loading
        {
            string powerupTag = GameCtrlr.instance.m_powerupTag;
            Material powerupMat = GameCtrlr.instance.m_currentMaterial;
            Customize(powerupTag, powerupMat);
        }
    }

    public void SetTag(string tag)
    {
        m_tag = tag;
    }
    public void ResetObstacle()
    {
        Customize(m_tag, m_mat);
    }
    public void Customize(string _tag, Material _mat)
    {
        transform.gameObject.tag = _tag;
        transform.GetComponent<MeshRenderer>().sharedMaterial = _mat;
    }
    void OnTriggerEnter(Collider collision)
    {
        // Debug.Log("Woooy");
        string thisTag = transform.gameObject.tag;
        // If Bullet entered
        if(collision.gameObject.name == "bullet")
        {
            int bulletLives = collision.transform.parent.GetComponent<BulletCtrlr>().m_lives;
            // If Bullet is of the same color this obstacle
            if(collision.gameObject.tag == thisTag)
            {
                // Get Bullet Lives
                bulletLives--;
                collision.transform.parent.GetComponent<BulletCtrlr>().SetLives(bulletLives);
                // Transform into Coin/Gem
                if(GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
                    GameCtrlr.instance.m_audioSource.PlayOneShot(m_explodingSound);
                GenerateCollect();
                //Destroy(transform.gameObject);
                // Debug.Log("Touched by Bro !");
            }
            else // Bullet tag is diff from Obstacle Tag : In this case we directly destroy bullet
            {
                Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab
            }

            if(bulletLives == 0)
            {
                Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab
            }
            // Destroy Bullet  : In any case when collided with a Obstacle
            //Destroy(collision.transform.parent.gameObject); // Bullet is inside its prefab

        }
        else if (collision.gameObject.name == "super_bullet")
        {
            // Later destroy at n(th) obstacle
        }
        else if (collision.gameObject.tag == "Player" && !collision.GetComponentInParent<CharacterCtrlr>().IsRolling())
        {
            transform.GetComponent<BoxCollider>().enabled = false;
            if(GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
                GameCtrlr.instance.m_audioSource.PlayOneShot(m_loseLifeSound);
            DecreasePlayerLives();
        }
        else
        {
            //  if(collision.gameObject.tag == m_tag)
            // {
            //     // Transform into Coin/Gem
            //     // Destroy(transform.gameObject);
            //     Debug.Log("Touched by Bro 1!");
            // }
        }

        if (collision.gameObject.tag == "Player" && collision.GetComponentInParent<CharacterCtrlr>().IsRolling())  //the player is rolling
        {
            // Transform into Coin/Gem
            if (GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
                GameCtrlr.instance.m_audioSource.PlayOneShot(m_explodingSound);
            GenerateCollect();
        }
    }

    void GenerateCollect()
    {   
         
        List<string> types = new List<string>(){"Coin", "Gem", "Heart", "Star"};
        
        List<Gameplay_prop_struct> proports = GameCtrlr.instance.m_currentLeveStruct.proports;
        string collectType = "Coin";

		int r = Random.Range(0, (100));
        for (int i = 0; i < proports.Count; i++)
        {
            int proport = proports[i].prop;
            if (r > 0 && r <= proport)
            {
                collectType = types[i];
                break;
            }
            else
            {
                r -= proport;
            }
        }
        
        // Debug.Log("coin");
        StartCoroutine(IWaitBeforeHiding(collectType));
    }

    IEnumerator IWaitBeforeHiding(string collectType)
    {
        m_mat = transform.GetComponent<MeshRenderer>().material;
        m_particleMat.color = m_mat.color;
        transform.GetComponent<BoxCollider>().enabled = false;
        transform.GetComponent<MeshRenderer>().enabled = false; // Body
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false; // Parts
        m_particle.Play();
        
        transform.parent.Find(collectType).gameObject.SetActive(true);

        yield return new WaitForSeconds(m_particle.main.startLifetime.constantMax);
        transform.gameObject.SetActive(false);

    }


    public IEnumerator DesactivateObstacle()
    {
        m_mat = transform.GetComponent<MeshRenderer>().material;
        m_particleMat.color = m_mat.color;
        transform.GetComponent<BoxCollider>().enabled = false;
        transform.GetComponent<MeshRenderer>().enabled = false; // Body
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false; // Parts
        m_particle.Play();

        yield return new WaitForSeconds(m_particle.main.startLifetime.constantMax);
        //if(transform != null)
        //{
        //    transform.gameObject.SetActive(false);
        //}
    }
    void DecreasePlayerLives()
    {
        GameCtrlr.instance.DecreasePlayerLives();
    }

}
