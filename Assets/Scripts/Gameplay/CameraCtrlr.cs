﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrlr : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 0.1f;
    public float speed = 3.0f;
    
    public float followDistance = 35f;
    public float verticalBuffer = 1.5f;
    public float horizontalBuffer = 0f;
    
    private Vector3 velocity = Vector3.zero;
    
    public Quaternion rotation = Quaternion.identity;
    
    public float yRotation = 0.0f;
    
    float m_currentXRotation;
    float m_stairXDownRot = 74.5f;
    float m_stairXUpRot = -0.74f;

    void Start()
    {
        m_currentXRotation = transform.eulerAngles.x;
    }
    void LateUpdate () {
        Vector3 targetPosition = target.TransformPoint(new Vector3(horizontalBuffer, followDistance, verticalBuffer));
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        //this is the code that solves the problem
        transform.eulerAngles = new Vector3(target.transform.eulerAngles.x, target.transform.eulerAngles.y, 0);

        //if (!target.transform.parent.GetComponent<Follower>().m_tiltCameraUp && !target.transform.parent.GetComponent<Follower>().m_tiltCameraDown)
        //{
        //    transform.eulerAngles = new Vector3(m_currentXRotation, target.transform.eulerAngles.y, 0);
        //}
        //else
        //{
        //    if(target.transform.parent.GetComponent<Follower>().m_tiltCameraDown)
        //    {
        //        transform.eulerAngles = new Vector3(m_stairXDownRot, target.transform.eulerAngles.y, 0);
        //    }
        //    if(target.transform.parent.GetComponent<Follower>().m_tiltCameraUp)
        //    {
        //        transform.eulerAngles = new Vector3(m_stairXUpRot, target.transform.eulerAngles.y, 0);
        //    }
        //}
        //------------------------------------------------------------------------
    }


    // The target we are following
    // public Transform target;
    // // The distance in the x-z plane to the target
    //     //So this would be your offset
    // public float distance = 10.0f;
    // // the height we want the camera to be above the target
    // public float height = 5.0f;
    // // How much we 
    // public float heightDamping = 2.0f;
    // public float rotationDamping = 3.0f;

    // // Place the script in the Camera-Control group in the component menu
    // // [AddComponentMenu("Camera-Control/Smooth Follow")]

    // void LateUpdate () {
    //     // Early out if we don't have a target
    //     if (!target) return;

    //     // Calculate the current rotation angles
    //     float wantedRotationAngle = target.eulerAngles.y;
    //     float wantedHeight = target.position.y + height;

    //     float currentRotationAngle = transform.eulerAngles.y;
    //     float currentHeight = transform.position.y;

    //     // Damp the rotation around the y-axis
    //     currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

    //     // Damp the height
    //     currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

    //     // Convert the angle into a rotation
    //     var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

    //     // Set the position of the camera on the x-z plane to:
    //     // distance meters behind the target
    //     transform.position = target.position;
    //     transform.position -= currentRotation * Vector3.forward * distance;

    //     // Set the height of the camera
    //     transform.position = new Vector3(transform.position.x,currentHeight,transform.position.z);

    //     // Always look at the target
    //     transform.LookAt(target);
    // }



}
