﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnCtrlr : MonoBehaviour
{
    // Start is called before the first frame update
    public float m_angle = 90.0f;
    void Start()
    {
        
    }

    public void Turn(Collider collision)
    {
        if(collision.tag.Equals("Player"))
        {
            Transform Player = collision.transform.parent; // Character is inside Player (which contains the CharacterCtrlr script)
            Player.GetComponent<CharacterCtrlr>().TurnPlayer(m_angle);
            // transform.GetChild(0).gameObject.SetActive(false);
        }
        if(collision.tag.Equals("BossPos"))
        {
            Debug.Log("follow up");
            collision.transform.parent.GetComponent<BossFollowUpPos>().TurnPlayer(m_angle);
        }

        if (collision.gameObject.GetComponent<BossCtrl>() != null)
        {
            collision.GetComponent<BossCtrl>().TurnPlayer(m_angle);
        }
    }

   
}
