﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGenerate : MonoBehaviour
{
    public GameObject m_roadPrefab;
    public Transform m_firstRoad;
    public Transform m_roadsContainer;
    
    [HideInInspector]
    public Transform m_lastRoad;
    // Start is called before the first frame update
    void Start()
    {
        m_lastRoad = m_firstRoad;
        // lastRoad = GenerateRoadShape(lastRoad);

        // for(int i=0; i < 5; i++)
        // {
        //     lastRoad = GenerateRoadShape(lastRoad);
        // }
    }

    public void GenerateRoadShape()
    {
        GameObject instantiatedRoad = Instantiate(m_roadPrefab, Vector3.zero, m_roadPrefab.transform.rotation, m_roadsContainer);

        // Setup Road
        int nextRoadShapeIndex = m_lastRoad.GetComponent<RoadPrefabCtrlr>().GetNextShapeIndex();
        Vector3 currentRoadEndPos = m_lastRoad.GetComponent<RoadPrefabCtrlr>().GetEndPos();
        //Debug.Log(currentRoadEndPos);
        Quaternion iRot = instantiatedRoad.transform.rotation;
        float yRot = instantiatedRoad.transform.GetComponent<RoadPrefabCtrlr>().SetActiveShape(nextRoadShapeIndex, currentRoadEndPos);
        // Debug.Log(yRot);
        m_lastRoad = instantiatedRoad.transform;
    }

    
}
