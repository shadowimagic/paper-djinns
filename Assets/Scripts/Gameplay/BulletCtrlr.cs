using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrlr : MonoBehaviour
{
    public int m_lives = 1;
    const float m_translation = 300f;
    float m_speed = 30f;
    float m_motion = 0f;
    float m_motionz = 0;
    GameObject m_character;
    float m_bulletBaseSpeed =   1200;
    float m_bulletSpeed     =   1.5f;
    bool  m_isDestroyed     =   false;
    float m_timeToLive      =   0.5f;
    Vector3 m_shootingDirection;
    public AudioClip m_loseLifeSound;

    // Start is called before the first frame update
    void Start()
    {
        // transform.GetComponent<Rigidbody>().velocity = Vector3.forward * (m_bulletBaseSpeed * m_bulletSpeed);
        Destroy(gameObject, m_timeToLive);
    }

    public void SetAngle(float angle)
    {   
        Rigidbody rb = transform.GetComponent<Rigidbody>();

        Vector3 destRotation = new Vector3(0, angle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        rb.rotation = finalRotation;
    }

    public void SetTimeToLive(float _timeToLive)
    {
        m_timeToLive = _timeToLive;
    }
    public void SetBulletSize(Vector3 _size)
    {
        transform.localScale = _size;
    }
    public void SetLives(int lives)
    {
        m_lives = lives;
    }
    public void SetMaterial(Material mat, string _tag = "")
    {
        string materialName = mat.name;
        string[] splitArray =  materialName.Split(char.Parse("_")); //
        string tag = splitArray[1];
        Transform bulletMesh = transform.GetChild(0);
        if(_tag.Equals(""))
        {
            transform.gameObject.tag = tag;
            bulletMesh.gameObject.tag = tag;
        }
        else
        {
            transform.gameObject.tag = _tag;
            bulletMesh.gameObject.tag = _tag;
        }
        bulletMesh.GetComponent<MeshRenderer>().sharedMaterial = mat;
    }

    public void SetBulletSpeed(float _speed)
    {
        m_speed = _speed;
    }

    void FixedUpdate()
    {
       transform.Translate(/*Vector3.forward*/m_shootingDirection * m_speed);
    }

    public void SetShootingDirection(Vector3 _direction)
    {
        m_shootingDirection = _direction;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if(this.gameObject.tag.Contains("_b") && collision.gameObject.tag == "Player")
        {

            if (GameCtrlr.instance.m_audioSource.gameObject.activeInHierarchy)
                GameCtrlr.instance.m_audioSource.PlayOneShot(m_loseLifeSound);
            DecreasePlayerLives();
        }
    }

    public float GetBulletSpeed()
    {
        return m_speed;
    }

    void DecreasePlayerLives()
    {
        GameCtrlr.instance.DecreasePlayerLives();
    }


}


