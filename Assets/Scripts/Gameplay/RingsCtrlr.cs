﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingsCtrlr : MonoBehaviour
{
    // Start is called before the first frame update
    float m_rotRatio = 25f;
    float m_rotSpeed = 1;

    [HideInInspector]
    public enum RotateDir
    {
        horizontal,
        vertical
    }

    public RotateDir m_rotateDir = RotateDir.vertical;

    void Start()
    {
        m_rotSpeed = GameCtrlr.instance.m_cylindreRotSpeed;        
    }
    
    void FixedUpdate()
    {
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {   
            float rotation = (m_rotRatio * Time.fixedDeltaTime);
            // if(m_rotateDir == RotateDir.vertical)
                transform.Rotate(0f, 0.0f,(rotation * m_rotSpeed), Space.Self);
            // else
            //     transform.Rotate(0f , 0.0f,(rotation * m_rotSpeed), Space.Self);

        }

    }
 
}
