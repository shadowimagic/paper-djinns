﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using TMPro;
using MyUtils;

public class GameCtrlr : MonoBehaviour
{
    public static GameCtrlr instance;
    
    public CharacterCtrlr m_characterCtrlr;
    public float m_cylindreRotSpeed = 0.5f;

    public GameObject m_selectionColorsBox;

    public AudioSource m_audioSource;

    public AudioClip m_levelupSound;
    public AudioClip m_gameoverSound;

    const float m_BASE_ROTATION_SPEED = 2f;
    bool m_gameOver = true;
    bool m_gamePaused = true;

    [HideInInspector]
    public int m_minObstacles = 1;
    [HideInInspector]
    public int m_maxObstacles = 3;
    [HideInInspector]
    public int m_ringsSpacing = 2; // 0:1:2 (0, 1 or 2 rings unit between two rings)
    // Start is called before the first frame update
    [HideInInspector]
    public int m_maxColors = 2; 
    
    public List<Material> m_obstaclesMaterials = new List<Material>();
    public List<Material> m_pickableObstaclesMaterials = new List<Material>();
    public Material m_currentMaterial;
    int m_currentMaterialIndex = 0;

    public string m_powerupTag;
    public bool m_isStarPowerupOn = false;
    public bool m_isBossGenerated = false; // To control Boss Generation props

    public int m_gunType = 1;
    public List<Sprite> m_gunsSprites;
    public Image m_gunIcon;
    public GameObject m_menuScreen;
    public GameObject m_bossScreen;
    public GameObject m_challengeScreen;
    public GameObject m_mapScreen;
    public GameObject m_multiplayerScreen;
    public GameObject m_bossRendererComponents;
    public GameObject m_gameplayScreen;
    public GameObject m_gameOverScreen;
    public GameObject m_reachedLevelScreen;
    public GameObject m_pauseScreen;
    public GameObject m_shopScreen;
    public GameObject m_rankScreen;
    public GameObject m_tapAreaScreen;
    public GameObject m_pauseBtn;

    public Transform m_coinsContainer;
    public GameObject m_coinPrefab;

    public Image m_coinProgressBar;
    public TextMeshProUGUI m_coinProgressText;
    public Image m_gemProgressBar;
    public TextMeshProUGUI m_gemProgressText;

    public Image m_livesProgress;

    public TextMeshProUGUI m_bonusText;
    public TextMeshProUGUI m_kmsText;

    public GameObject m_bonusCoinIcon;
    public GameObject m_bonusGemIcon;
    public GameObject m_bonusHealthIcon;
    public GameObject m_bonusStarIcon;

    public GameObject m_soundBtn;

    public List<Sprite> m_soundSprites;

    public TextMeshProUGUI m_starPowerupCdText;

    public GameObject m_swipeIcon;

    private string m_gameplay_struct_path = "Gameplay";
    private TextAsset m_json;

    Gameplay_struct m_gameplay_struct;

    public Gameplay_level_struct m_currentLeveStruct;

    public int m_currentLevel = 1;
    public bool m_reset = false;

    public bool m_isCharacterBlinking = false;

    ////////////// CAMERA SHAKING EFFECT /////////////////

    Transform m_camTransform;
	
	// How long the object should shake for.
	float m_shakeDuration = 0f;
	
	// Amplitude of the shake. A larger value shakes the camera harder.
	float m_shakeAmount = 0.7f;
	float m_decreaseFactor = 1.0f;
	Vector3 m_originalPos;
    public bool m_isBossActivated;
    public GameObject m_gameBackground; //game background
    public GameObject m_bossFollowUp;
    //Level loader Components 
    public Image m_levelLoaderImg;  //level loader^progression
    public GameObject m_currentLevelCpomponents;
    public TextMeshProUGUI m_currentLevelIndex;
    public TextMeshProUGUI m_nextLevelIndex;

    ///////////// GAME OVER SCREEN INFORMATION ////////
    public TextMeshProUGUI m_collectedCoinsGSText;  //displayed the collected coins
    public TextMeshProUGUI m_collectedGemsGSText;   //displayed the collected gems
    public TextMeshProUGUI m_distanceTraveledGSText;    //displayed the traveled distance
    public List<Image> m_unlockedStarsGS;   //displayed the unlock stars
    public TextMeshProUGUI m_origamisLiberatedGSText;   //show the number of liberated origamis
    public Image m_progressionFillGS;   //show the level progress fill
    public TextMeshProUGUI m_currentLevelIndexGSText;   //show the current level index
    public TextMeshProUGUI m_nextLevelIndexGSText;  //show the next level index
    public TextMeshProUGUI m_rankGSText;    //show the current rank
    public TextMeshProUGUI m_playerNameText;    //player name text
    public PersistentInt m_currentHealth = new PersistentInt("HEALTH", 3);
    public LevelCtrlr m_levelCtrl;  //contains the informations of the current level

    //////////////////////////////////////////////////////

    void Awake()
    {
        // timer = time * 60f;
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        // LoadGamePlay
        LoadGamePlay();
        // Camera Shake
        m_camTransform = Camera.main.transform;
    }


    void OnEnable()
	{
		m_originalPos = m_camTransform.localPosition;
	}

    void LoadGamePlay()
    {
        if(!PlayerPrefs.HasKey("gameplay") || m_reset)
        {
            string gameplayPath = m_gameplay_struct_path + Path.DirectorySeparatorChar + "GameplayData";

            TextAsset json = Resources.Load(gameplayPath) as TextAsset;

            if (json != null)
            {
                m_gameplay_struct = JsonUtility.FromJson<Gameplay_struct>(json.text);
                PlayerPrefs.SetString("gameplay", json.text);
            }
            else
            {
                //Debug.LogError("Cannot find file: " + modulePath);
            }

            if(m_reset)
            {
                ResetAll();
            }

        }
        else
        {
            m_gameplay_struct = JsonUtility.FromJson<Gameplay_struct>(PlayerPrefs.GetString("gameplay"));
        }
        
        //  Define presets
    }

    void ResetAll()
    {
        PlayerPrefs.SetInt("currentLevel",1);
        PlayerPrefs.SetInt("coins", 0);
        PlayerPrefs.SetInt("gems", 0);
        PlayerPrefs.SetInt("guntype", 1);
        PlayerPrefs.SetInt("kms", 0);
    }

    void DefineGameplayPresets()
    {
        //  Define props
        m_currentLevel = PlayerPrefs.GetInt("currentLevel",1);
        SetLevelLoaderValues(m_currentLevel);   //set level loader index values
        m_currentLeveStruct = m_gameplay_struct.levels[(m_currentLevel-1)]; // index start from 0
        m_cylindreRotSpeed = (m_currentLeveStruct.ringsSpeed * m_BASE_ROTATION_SPEED);
        m_minObstacles = m_currentLeveStruct.minObstacles;
        m_maxObstacles = m_currentLeveStruct.maxObstacles;
        m_ringsSpacing = m_currentLeveStruct.ringsSpacing;
        m_maxColors = m_levelCtrl.m_levelStruct.m_maxColors;    //get the max number of colours from the level

        GetRandomNumberOfColours(m_maxColors);

        // Define SelectedColorsBox
        DefineSelectionColorsBox();

        // Define Gun Type
        m_gunType = PlayerPrefs.GetInt("gunType", 1);
        CheckLevelUp();

    }
    void SetLevelLoaderValues(int _currentLevelIndex)
    {
        m_currentLevelIndex.text = _currentLevelIndex.ToString();
        m_nextLevelIndex.text = (_currentLevelIndex + 1).ToString();
    }
    
    void DisplaySwipeIcon()
    {
        if(PlayerPrefs.GetString("swiped", "false") == "false")
        {
            StartCoroutine(IDisplaySwipeIcon());
        }
    }
    IEnumerator IDisplaySwipeIcon()
    {
        yield return new WaitForSeconds(1);
        m_swipeIcon.SetActive(true);
        yield return new WaitForSeconds(3);
        m_swipeIcon.SetActive(false);
        PlayerPrefs.SetString("swiped", "true");

    }
    public void SetGunType(int gt)
    {
        m_gunType = gt;
        PlayerPrefs.SetInt("guntype", gt);
    }

    void DefineSelectionColorsBox()
    {
        m_selectionColorsBox.transform.GetComponent<SelectedColorsCtrlr>().DefineBoxes(m_pickableObstaclesMaterials);
    }
    public bool IsGameOver()
    {
        return m_gameOver;
    }
    public bool IsGamePaused()
    {
        return m_gamePaused;
    }

    public void StartGameplay()
    {
        //if(m_currentHealth.Get() > 0)
        {
            m_menuScreen.SetActive(false);
            m_gameOverScreen.SetActive(false);
            m_gameplayScreen.SetActive(true);

            int soundInt = PlayerPrefs.GetInt("sound", 1);
            m_audioSource.gameObject.SetActive(soundInt == 1);

            ChangeBulletColor();
            // GunType
            m_gunIcon.sprite = m_gunsSprites[m_gunType - 1];

            // Check Levelup
            CheckLevelUp();
        }
    }

    public void Go()
    {
        m_gameOver = false;
        m_gamePaused = false;

        // Reset
        PlayerPrefs.SetString("restart", "false");

        // int soundInt = PlayerPrefs.GetInt("sound", 0);
        // m_audioSource.gameObject.SetActive(soundInt == 1);
        
        // Swipe intro
        DisplaySwipeIcon();

    }

    public void Restart(string restarting = "true")
    {
        PlayerPrefs.SetString("restart", restarting);
        Destroy(gameObject);
        SceneManager.LoadScene("NewGameplay");
        //PlayerPrefs.SetString("restart", "false");

    }
    void Start()
    {
        ToggleSound();


        // Toggle screens
        m_menuScreen.SetActive(true);
        m_gameOverScreen.SetActive(false);
        m_gameplayScreen.SetActive(false);


        // Pause sound
        m_audioSource.gameObject.SetActive(false);

        if(PlayerPrefs.GetString("restart", "false") == "true")
        {
            StartGameplay();
            // Debug.Log("restarted");
        }
        m_levelCtrl.LoadLevel();    //load the current level
        // Check level up
        DefineGameplayPresets();
        CheckLevelUp(); // Defining too stats
        //SetCurrentMaterial(0); // Red
        ChangeBulletColor();
        m_playerNameText.text = Player.Instance.m_playerName.Get();        //set the player name
    }

    // This must get (max - (currentNumberOfColors)) without selecting a existing one
    void GetRandomNumberOfColours(int max)
    {
        int maxColorsToPick = max - m_pickableObstaclesMaterials.Count;

        if(maxColorsToPick > 0)
        {

            int counter = 0;
    
            while(counter < maxColorsToPick)
            {
                int randomColourIndex = Random.Range(0, m_obstaclesMaterials.Count);
                Material mat = m_obstaclesMaterials[randomColourIndex];
                if(m_pickableObstaclesMaterials.IndexOf(mat) == -1)
                {
                    m_pickableObstaclesMaterials.Add(mat);
                    counter++;
                }
            }
        
        }

        // return selectedMaterials;
    }

    void SetCurrentMaterial(int index)
    {
        m_currentMaterial = m_pickableObstaclesMaterials[index]; // Red
        m_currentMaterialIndex = index;
    }
    public void ChangeBulletColor()
    {
        int nextMaterialIndex = m_currentMaterialIndex + 1;
        if(nextMaterialIndex == m_pickableObstaclesMaterials.Count)
            nextMaterialIndex = 0;
        SetCurrentMaterial(nextMaterialIndex);
        m_selectionColorsBox.transform.GetComponent<SelectedColorsCtrlr>().ActivateColor(nextMaterialIndex);
        Color c = m_pickableObstaclesMaterials[nextMaterialIndex].color;
        m_characterCtrlr.SetBulletBarColor(m_pickableObstaclesMaterials[nextMaterialIndex]);  //set bullet bar color
        m_tapAreaScreen.transform.GetComponent<TapAreaCtrlr>().ChangeBtnColor(c);
    }


    void CheckLevelUp()
    {

        int coins = m_characterCtrlr.m_coins;
        int gems = m_characterCtrlr.m_gems;
        int coinsBase = 1000;
        int gemsBase  = 100;
        if(m_currentLevel == 1) 
        {
            coinsBase = 300;
            gemsBase  = 20;
        }
        int thisLevelPassCoins = m_currentLevel * coinsBase;
        int thisLevelPassGems = m_currentLevel * gemsBase;

        // Progression -------

        float coinLevelPercentage = (float)coins / (float)thisLevelPassCoins;
        float gemLevelPercentage = (float)gems / (float)thisLevelPassGems;
        
        m_coinProgressBar.transform.GetComponent<Image>().fillAmount = coinLevelPercentage;
        m_gemProgressBar.transform.GetComponent<Image>().fillAmount = gemLevelPercentage;

        m_coinProgressText.text = coins + " / " + thisLevelPassCoins;
        m_gemProgressText.text = gems + " / " + thisLevelPassGems;

        float coinLevelLoaderP = 0f;
        float gemLevelLoaderP = 0f;

        if(coins > thisLevelPassCoins)
        {
            coinLevelLoaderP = 1f;
        }
        else
        {
            coinLevelLoaderP = coinLevelPercentage;
        }

        if(gems > thisLevelPassGems)
        {
            gemLevelLoaderP = 1f;
        }
        else
        {
            gemLevelLoaderP = gemLevelPercentage;
        }
        m_levelLoaderImg.fillAmount = (coinLevelLoaderP + gemLevelLoaderP) / 2;//(float)(coins + gems) / (float)(thisLevelPassCoins + thisLevelPassGems);
        MoveLevelIndexIndicator();//make current level index follow level progression
        // -------------------

        //if ((coins >= thisLevelPassCoins) && (gems >= thisLevelPassGems))
        //{
        //    GenerateBoss();
        //    //UnlockNextLevel();
        //}

    }

    public void MoveLevelIndexIndicator()
    {
        if(m_levelLoaderImg.fillAmount <= 1)
        {
            m_currentLevelCpomponents.GetComponent<RectTransform>().localPosition = new Vector3(m_levelLoaderImg.fillAmount * m_levelLoaderImg.GetComponent<RectTransform>().sizeDelta.x - 80, m_currentLevelCpomponents.GetComponent<RectTransform>().localPosition.y, m_currentLevelCpomponents.GetComponent<RectTransform>().localPosition.z);
        }
    }

    public void GetCoin(int type, int point)
    {
        // Effects
        if(point > 0) // Points type : Else heart
        {
            m_bonusText.text = "+" + point;
            if(m_bonusText.gameObject.activeInHierarchy)
                m_bonusText.gameObject.SetActive(false);
            m_bonusText.gameObject.SetActive(true);

            if(type == 1) // Coin
            {        
                if(m_bonusCoinIcon.gameObject.activeInHierarchy)
                    m_bonusCoinIcon.gameObject.SetActive(false);
                m_bonusCoinIcon.gameObject.SetActive(true);
            }

            if(type == 2) // Gem
            {        
                if(m_bonusGemIcon.gameObject.activeInHierarchy)
                    m_bonusGemIcon.gameObject.SetActive(false);
                m_bonusGemIcon.gameObject.SetActive(true);
            }
        }
        else
        {
            if(type == 3) // Heart
            {        
                if(m_bonusHealthIcon.gameObject.activeInHierarchy)
                    m_bonusHealthIcon.gameObject.SetActive(false);
                m_bonusHealthIcon.gameObject.SetActive(true);
                
            }
            else if(type == 4) // Star
            {
                if(m_bonusStarIcon.gameObject.activeInHierarchy)
                    m_bonusStarIcon.gameObject.SetActive(false);
                m_bonusStarIcon.gameObject.SetActive(true);
            }
        }

        m_characterCtrlr.GetCoin(type, point);

        CheckLevelUp();
    }

    public void ToggleMotionOnInjured(bool status)
    {
        m_gamePaused = status;
    }

    // Instantiate a Boss on the next slot
    public void GenerateBoss()
    {
        if(!m_isBossActivated)
        {
            Debug.Log("Genrating boss...");
            m_isBossActivated = true;   //activated the boss (variable which allows us to stopped certains events while the boss fight is happening)
            DesactivateAllObstacles();//should desactivate all obstacles 
            StartCoroutine(InstantiateBoss(m_characterCtrlr.gameObject));//spawn the boss 10 feet away from the player
        }
    }


    public void DefineBossFightSettings()
    {

        m_maxColors = m_currentLeveStruct.boss.nbrBodyColors;

        GetRandomNumberOfColours(m_maxColors);

        // Define SelectedColorsBox
        DefineSelectionColorsBox();
    }
    public void DesactivateAllObstacles()
    {
        foreach(ObstacleCtrlr o in  FindObjectsOfType<ObstacleCtrlr>())
        {
            StartCoroutine(o.DesactivateObstacle());
        }
    }
    public IEnumerator InstantiateBoss(GameObject _player)
    {
        yield return new WaitForSeconds(0.3f);
        StartCoroutine(AudioManager.Instance.fadeInBossSoundtrack());
        m_gameBackground.GetComponent<Animator>().SetTrigger("BOSS_APPEARED");
        yield return new WaitForSeconds(1.5f);  //wait half second before the boss appeared
        GameObject _boss = Instantiate(Resources.Load("Bosses/"+ m_currentLeveStruct .boss.fileName+"")/*, _player.transform.parent*/) as GameObject;
        DataBackup.Instance.InitBossKey(m_currentLeveStruct.boss.displayName);  //init the 
        DefineBossFightSettings();
        Debug.Log(new Vector3(_player.transform.GetChild(0).position.x, _player.transform.GetChild(0).position.y + 44f, _player.transform.GetChild(0).position.z + 500f));
        //_player.transform.parent.GetChild(_player.transform.parent.childCount - 1).transform.position = new Vector3(_player.transform.position.x, _player.transform.position.y, _player.transform.position.z); //use new vector3
        _boss.transform.rotation = m_bossFollowUp.transform.rotation;
        _boss.transform.GetChild(2).GetChild(0).GetComponent<BossCtrl>().SetRotationAngle(m_bossFollowUp.transform.parent.GetComponent<BossFollowUpPos>().GetCurrentRotationAngle());
        //_boss.transform.position = new Vector3(_player.transform.GetChild(0).position.x, _player.transform.GetChild(0).position.y + 44f, _player.transform.GetChild(0).position.z + 500f);
        _boss.transform.GetChild(2).GetChild(0).GetComponent<BossCtrl>().m_bossInitialPosition = new Vector3(_player.transform.GetChild(0).position.x, _player.transform.GetChild(0).position.y + 44f, _player.transform.GetChild(0).position.z + 500f);
        _boss.transform.SetParent(_player.transform.parent.GetChild(_player.transform.parent.childCount - 1), true);
        _boss.transform.GetChild(2).GetChild(0).GetComponent<BossCtrl>().SetBossMaterial();
        _boss.transform.GetChild(2).GetChild(0).GetComponent<BossCtrl>().SetBulletMaterial(_boss.transform.GetChild(2).GetChild(0).GetComponent<BossCtrl>().GetBossMaterial());
    }

    public void UnlockNextLevel()
    {
        // m_gameOver = true;
        // m_gamePaused = true;

        if((m_currentLevel + 1) < m_gameplay_struct.levels.Count)
        {
            DataBackup.Instance.SetEarnedStarOnBossFight(m_currentLeveStruct.boss.displayName,RankCtrl.Instance.GetNumberOfStarsBasedOnPlayerHealth(m_characterCtrlr.m_lifeBar));
            // Setup next level presets
            m_currentLevel++;
            PlayerPrefs.SetInt("currentLevel", (m_currentLevel));
            // Unlock Next Config
            
            // Goto Congrats Screen
            StartCoroutine(IDisplayCongrats());

            // Define gameplay preset
            DefineGameplayPresets();
        }
        else
        {

        }
     
    }

    void DisplayCongrats(bool status)
    {
        m_reachedLevelScreen.SetActive(status);
    }

    IEnumerator IDisplayCongrats()
    {
        // Update Level
        m_characterCtrlr.NextLevel((m_currentLevel));

        DisplayCongrats(true);

        m_audioSource.PlayOneShot(m_levelupSound);

        yield return new WaitForSeconds(1.0f);
        DisplayCongrats(false);
    }
    public void DecreasePlayerLives()
    {
        // m_shakeDuration = 2.0f;
        m_characterCtrlr.DecreasePlayerLives();
    }

    public void IncreaseHeartLoader(int lives)
    {
        float value = (1.0f / 4.0f)  * lives;
        m_livesProgress.fillAmount = value; 
        // StartCoroutine(IIncreaseHeartLoader(value));
    }


    IEnumerator IIncreaseHeartLoader(float value)
    {   
        float duration = 10f;
        float initValue = m_livesProgress.fillAmount;
        float progress = 0;
        float diff = value - initValue;
        float step = diff/duration;
        while (progress <= diff)
        {
            progress += step;
            m_livesProgress.fillAmount += (progress * Time.deltaTime);

            if(progress >= diff)
            {
                m_livesProgress.fillAmount = value;
            }
        }

        yield return 0; 
        
    }

    public void StartStarPowerup()
    {
        // Get current ball material (V)
        // Get all obstacles (by script) and set their new mat and tag
        // Display starPowerupCountDown et Start countdown
        
        m_isStarPowerupOn = true;

        m_powerupTag = m_currentMaterial.name;
        string[] splitArray =  m_powerupTag.Split(char.Parse("_")); //
        m_powerupTag = splitArray[1];

        ObstacleCtrlr[] allObstacles = FindObjectsOfType<ObstacleCtrlr>();
        foreach (ObstacleCtrlr obst in allObstacles)
        {
            obst.Customize(m_powerupTag, m_currentMaterial);
        }

        // Display Countdown and Start It
        StartStarPowerupCountdown();

        // Start Waiting
        StartCoroutine(IWaitBeforFinishingStarPowerup());
        
    }
    

    IEnumerator IWaitBeforFinishingStarPowerup()
    {
        yield return new WaitForSeconds(11) ;// seconds
        m_isStarPowerupOn = false;
        ObstacleCtrlr[] allObstacles = FindObjectsOfType<ObstacleCtrlr>();
        foreach (ObstacleCtrlr obst in allObstacles)
        {
            obst.ResetObstacle();
        }

    }

    void StartStarPowerupCountdown()
    {
        m_starPowerupCdText.gameObject.SetActive(true);
        int count = 10;
        StartCoroutine(CountDown(count));
    }

    IEnumerator CountDown(int _countdown)
    {   
        float cdDuration = 1f;
        m_starPowerupCdText.text = _countdown.ToString();
        yield return new WaitForSeconds(cdDuration); 
        _countdown--;
        if(_countdown > 0)
        {
            StartCoroutine(CountDown(_countdown));
        }
        else
        {
            m_starPowerupCdText.text = _countdown.ToString();
            yield return new WaitForSeconds(0.25f);
            m_starPowerupCdText.gameObject.SetActive(false);

        }
    }

    public void SetKms(float value)
    {
        m_kmsText.text = Mathf.Round(value) + " KM";
    }
    public void GameOver()
    {
         
        m_audioSource.PlayOneShot(m_gameoverSound);
        StartCoroutine(IWaitBeforeStopingSound());
        m_gameOver = true;
        m_gamePaused = true;

        // Toggle screens
        m_menuScreen.SetActive(false);
        m_gameOverScreen.SetActive(true);
        SetGameOverScreenInformations();
        m_gameplayScreen.SetActive(false);
    }

    public void GotoMenu()
    {
        // PlayerPrefs.SetString("restart", "false");
        // SceneManager.LoadScene("Gameplay");
        // m_menuScreen.SetActive(false);

        Restart("false");

    }

    public void GotoShop()
    {
        m_menuScreen.SetActive(false);
        m_shopScreen.SetActive(true);

        // m_shopScreen.transform.GetComponent<ShopScreenCtrlr>().SetupUI();
    }

    public void FromShopToMenu()
    {
        m_menuScreen.SetActive(true);
        m_shopScreen.SetActive(false);
    }
    public void ToggleRankUI(bool status)
    {
        m_menuScreen.SetActive(!status);
        m_rankScreen.SetActive(status);
    }

    public void ToggleBossScreen(bool status)
    {
        m_menuScreen.SetActive(!status);
        m_bossScreen.SetActive(status);
        m_bossRendererComponents.SetActive(true);
        if (status)
            m_bossScreen.GetComponent<BossUICtrl>().PopulateBossUIContainer();
    }

    public void ToggleChallengeScreen(bool status)
    {
        m_menuScreen.SetActive(!status);
        m_challengeScreen.SetActive(status);
    }

    public void ToggleMapScreen(bool status)
    {
        m_menuScreen.SetActive(!status);
        m_mapScreen.SetActive(status);
    }

    public void ToggleMultiplayerScreen(bool status)
    {
        m_menuScreen.SetActive(!status);
        m_multiplayerScreen.SetActive(status);
    }
    IEnumerator IWaitBeforeStopingSound()
    {
        yield return new WaitForSeconds(1.5f);
        m_audioSource.gameObject.SetActive(false);
    }

    public void PauseGame()
    {
        m_gameOver = false;
        m_gamePaused = true;
        m_audioSource.gameObject.SetActive(false);
        m_pauseBtn.SetActive(false);

        // Toggle screens
        m_pauseScreen.SetActive(true); 
    }
    public void ResumeGame()
    {
        m_gameOver = false;
        m_gamePaused = false;
        
        if(PlayerPrefs.GetInt("sound", 0) == 1)
            m_audioSource.gameObject.SetActive(true);
        m_pauseBtn.SetActive(true);

        // Toggle screens
        m_pauseScreen.SetActive(false); 
    }

    public void ToggleSound(bool autoToggle = true)
    {
        int soundState = PlayerPrefs.GetInt("sound", 1);
        if(!autoToggle)
        {
            soundState = 1 - soundState; // 1--> 0; 0--> 1
        }
        PlayerPrefs.SetInt("sound", soundState);
        m_soundBtn.GetComponent<Image>().sprite = m_soundSprites[soundState];
    }


    public void SetGameOverScreenInformations()
    {
        m_currentHealth.Set(0);
        m_collectedCoinsGSText.text = m_characterCtrlr.m_coins.ToString();
        m_collectedGemsGSText.text = m_characterCtrlr.m_gems.ToString();
        m_distanceTraveledGSText.text = Mathf.Round(m_characterCtrlr.m_kms).ToString() + " KM";
        ActivateGameOverScreenUnlockedStars(RankCtrl.Instance.m_nbrActivatedStars);
        SetLevelProgressValueGameOverScr();
        m_rankGSText.text = RankCtrl.Instance.m_currentRank.rank;
    }

    public void SetLevelProgressValueGameOverScr()
    {
        m_progressionFillGS.fillAmount = m_levelLoaderImg.fillAmount;
        m_currentLevelIndexGSText.text = m_currentLevelIndex.text;
        m_nextLevelIndexGSText.text = m_nextLevelIndex.text;
        m_currentLevelIndexGSText.transform.parent.GetComponent<RectTransform>().localPosition = new Vector3(m_progressionFillGS.fillAmount * m_progressionFillGS.GetComponent<RectTransform>().sizeDelta.x - 80, m_currentLevelIndexGSText.transform.parent.GetComponent<RectTransform>().localPosition.y, m_currentLevelIndexGSText.transform.parent.GetComponent<RectTransform>().localPosition.z);
    }

    public void ActivateGameOverScreenUnlockedStars(int _numberOfUnlockedStars)
    {
        for(int i = 0; i < _numberOfUnlockedStars; i++)
        {
            m_unlockedStarsGS[i].gameObject.SetActive(true);
        }
    }

    public List<Gameplay_level_struct> GetListOfLevels()
    {
        return m_gameplay_struct.levels;
    }
}

[System.Serializable]
public class Boss_struct
{
    public string fileName;
    public string displayName;
    public int lifeBars;
    public int nbrBodyColors;
    public int nbrBulletsColor;
    public float actionDelay;
    public float bulletSpeed;
    public float shiftingDelay;
}

[System.Serializable]
public class Gameplay_prop_struct
{
    public int prop = 25;
}
[System.Serializable]
public class Gameplay_level_struct
{
    public int levelId = 1;

    public int minObstacles = 1;
    public int maxObstacles = 1;
    public int colors = 2;
    public int ringsSpacing = 2;
    public int ringsSpeed = 1;
    public List<Gameplay_prop_struct> proports;
    public Boss_struct boss;

}

[System.Serializable]
public class Gameplay_struct
{
    public List<Gameplay_level_struct> levels;
}


