﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractorCtrlr : MonoBehaviour
{
    float m_attractionSpeed = 12.0f;
    // Start is called before the first frame update
    private void OnTriggerStay(Collider collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            transform.position = Vector3.MoveTowards(transform.position, collision.transform.position, m_attractionSpeed);
        }
    }

    void Update()
    {
        if(transform.childCount < 1)
        {
            Destroy(gameObject);
        }
    }
}
