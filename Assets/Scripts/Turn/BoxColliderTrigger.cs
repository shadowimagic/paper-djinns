﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxColliderTrigger : MonoBehaviour
{
   
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag.Equals("Player") || collision.gameObject.GetComponent<BossCtrl>() != null || collision.gameObject.tag.Equals("BossPos"))
        {
            gameObject.GetComponentInParent<TurnCtrlr>().Turn(collision);
            if(collision.gameObject.tag.Equals("Player"))
            {
                StartCoroutine(DesactivateBoxCollider());
            }
        }
    }

    public IEnumerator DesactivateBoxCollider()
    {
        GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSeconds(1f);
        GetComponent<BoxCollider>().enabled = true;
    }
}
