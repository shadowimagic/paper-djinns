﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class Follower : MonoBehaviour
{
    public PathCreator m_pathCreator;
    public Transform m_camera;
    public float m_speed = 5;
    float m_speedTurningPointRight = 2.5f;
    float m_speedTurningPointLeft = 0.7f;
    float m_distanceTravelled;
    public string m_currentParentPathContainer;
    public bool m_tiltCameraUp;
    public bool m_tiltCameraDown;
    //Player rotation
    private float m_rotationAngle = 90f;
    private Quaternion m_targetRot;
    private bool m_isPlayerRot;
    public bool m_isTurning;
    // Update is called once per frame

    private void Start()
    {
        //transform.localPosition = new Vector3(transform.localPosition.x, 413f, transform.localPosition.z);
    }

    void FixedUpdate()
    {
        if(m_pathCreator != null)
        {
            m_isTurning = true;
            m_distanceTravelled += m_speed * Time.deltaTime;
            transform.position = m_pathCreator.path.GetPointAtDistance(m_distanceTravelled, EndOfPathInstruction.Stop);
            transform.rotation = m_pathCreator.path.GetRotationAtDistance(m_distanceTravelled, EndOfPathInstruction.Stop);
            //Debug.Log(transform.position + " = " + m_pathCreator.path.GetPoint(m_pathCreator.path.NumPoints - 1) + " " + Vector3.Distance(transform.position, m_pathCreator.path.GetPoint(m_pathCreator.path.NumPoints - 1)));
            if (Vector3.Distance(transform.position, m_pathCreator.path.GetPoint(m_pathCreator.path.NumPoints - 1)) <= 0.001f)
            {
                m_isTurning = false;
                m_pathCreator = null;
                m_tiltCameraDown = false;
                m_tiltCameraUp = false;
                m_currentParentPathContainer = "";
                m_distanceTravelled = 0f;
            }
        }

        if(m_isPlayerRot)   //verified if the player is rotating
        {
            RotatePlayer(); //rotated the player
        }
    }

    public void RotatePlayer()
    {
        if (Mathf.Approximately(Mathf.Abs(Quaternion.Dot(transform.rotation, m_targetRot)), 1.0f))
        {
            m_isPlayerRot = false;
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, m_targetRot, Time.deltaTime * 5f);
    }

    public void RemovePathCreator()
    {
        m_pathCreator = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(gameObject.name);
        if(other.gameObject.tag.Equals("path")) //verified if the trigger object is the path
        {
            m_currentParentPathContainer = other.gameObject.name.Replace("(Clone)", "");
            if(m_currentParentPathContainer.Equals("TurningPointRight"))
            {
                m_speed = m_speedTurningPointRight;
            }
            else
            {
                m_speed = m_speedTurningPointLeft;
            }

            if (m_currentParentPathContainer.Equals("StairDown"))
            {
                m_tiltCameraDown = true;
            }
            m_pathCreator = other.transform.Find("Path_" + transform.parent.GetComponent<CharacterCtrlr>().m_pos).GetComponent<PathCreator>();   //set the path creator

        }

        if(other.gameObject.tag.Equals("crossroad"))
        {
            if(transform.parent.GetComponent<CharacterCtrlr>().m_pos == 1 && transform.parent.GetComponent<CharacterCtrlr>().m_isSwipingRight)//verified the current player pos is at the right
            {
                m_isPlayerRot = true;
                m_targetRot = Quaternion.Euler(0f, m_rotationAngle, 0f);
                transform.parent.GetComponent<CharacterCtrlr>().m_pos = int.Parse(other.gameObject.name);
            }
            if(transform.parent.GetComponent<CharacterCtrlr>().m_pos == -1 && transform.parent.GetComponent<CharacterCtrlr>().m_isSwipingLeft)//verified the current player pos is at the right
            {
                m_isPlayerRot = true;
                m_targetRot = Quaternion.Euler(0f, -m_rotationAngle, 0f);
                transform.parent.GetComponent<CharacterCtrlr>().m_pos = int.Parse(other.gameObject.name);
            }
        }
    }
}
