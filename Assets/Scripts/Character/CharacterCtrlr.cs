﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using MyUtils;

enum ORIENTATION
{
    STRAIGHT = 1,
    TURN = 2
}
public class CharacterCtrlr : MonoBehaviour
{
    public const int MAX_HEALTH = 3;
    public GameObject m_bulletPrefab;
    public Transform m_bulletContainer;
    public TextMeshProUGUI m_coinsText;
    public TextMeshProUGUI m_gemsText;
    public TextMeshProUGUI m_livesText;
    public TextMeshProUGUI m_levelText;
    public TextMeshProUGUI m_levelReachedText;
    public GameObject m_playerItem;

    public Rigidbody m_rb;
    
    Vector3 m_forward = new Vector3(0, 0, 1);
    Vector3 m_camForward = new Vector3(0, 0, 1);

    public float m_moveSpeed = 17;

    const float m_translation = 300f;
    const float m_speed = 2f;
    float m_motion = 0f;
    float m_motionDir = 0;
    GameObject m_character;
    Camera m_cam;
    float m_camXOffset = 0f;
    float m_bulletSpeed = 2;
    float m_shootRate = 5;
    float m_lastShooted;
    bool m_shoot = false;

    public int m_coins = 0;
    public int m_gems = 0;
    public int m_lives = 3;
    int m_collectedLives = 0;
    public float m_kms = 0;
    const float m_BASE_KM = 0.1f;
    
    private Vector2 m_lastPosition;
    
    public int m_pos = 0;
    int m_bulletLives = 1;

    bool m_turnLerp = false;
    bool m_turnCameraLerp = false;

    ORIENTATION m_orientation = ORIENTATION.STRAIGHT;
    int m_orientationSide = 1;
    float m_currentOrientationAngle = 0;

    private Vector3 m_fp;   //First touch position
    private Vector3 m_lp;   //Last touch position
    private float m_dragDistance;  //minimum distance for a swipe to be registered
    //remaining bullets manager
    public int m_numberOfBullets = 6;
    public int m_maxNumberOfBullets = 6;
    public List<Image> m_bulletsBar;
    public float m_reloadDelay = 0.5f;
    public float m_reloadDelayTracker;

    //Life bar 
    public List<Image> m_lifeBar;
    public List<Sprite> m_heartFillList;
    public int m_heartFillStep;
    public Image m_heartImage;
    public GameObject m_healthBarNotif;
    public bool m_isSwipingRight;
    public bool m_isSwipingLeft;
    public List<CharacterPos> m_characterPos;   //list of position that the character can occupied
    // Start is called before the first frame update
    void Start()
    {
        m_cam = Camera.main;
        m_camXOffset = m_cam.transform.position.z - transform.position.z;
        
        m_motion = (m_translation * m_speed);
        m_character = transform.GetChild(0).gameObject;        
        // Init
        m_coins = PlayerPrefs.GetInt("coins",0);
        m_gems = PlayerPrefs.GetInt("gems",0);
        m_lives = PlayerPrefs.GetInt("lives", 3);
        //m_lives = GameCtrlr.instance.m_currentHealth.Get();
        m_kms = PlayerPrefs.GetInt("kms", 0);

        if (m_lives == 0)
        {
            m_lives = 3;
            PlayerPrefs.SetInt("lives", m_lives);
        }

        m_coinsText.text = m_coins.ToString();
        m_gemsText.text = m_gems.ToString();
        m_livesText.text = m_lives.ToString();
        m_levelText.text = GameCtrlr.instance.m_currentLevel.ToString();
        GameCtrlr.instance.SetKms(m_kms);
        // Get Bullet lives
        m_bulletLives = GameCtrlr.instance.m_gunType;
        m_dragDistance = Screen.height * 2 / 100; //dragDistance is 5% height of the screen
        //SetHealthBarNotif();
        //InitHealthBar();
    }

    public void SetHealthBarNotif()
    {
        if(m_lives > MAX_HEALTH)
        {
            int diff = m_lives - MAX_HEALTH;
            m_healthBarNotif.SetActive(true);
            m_healthBarNotif.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = diff.ToString();
        }
        else
        {
            m_healthBarNotif.SetActive(false);
        }
    }

    public bool GetTurnLerpValue()
    {
        return m_turnLerp;
    }


    public void GenerateBullet()
    {
        if(m_numberOfBullets > 0 && !IsRolling())   //verified if the number of bullets is not equal to zero
        {
            GameObject instantiatedBullet = Instantiate(m_bulletPrefab, m_bulletContainer.position, m_bulletPrefab.transform.rotation);
            Material currentMaterial = GameCtrlr.instance.m_currentMaterial;
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetAngle(transform.GetChild(0).localRotation.eulerAngles.y);
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetMaterial(currentMaterial);
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetLives(m_bulletLives);
            instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetShootingDirection(Vector3.forward);
            DecreasedNumberOfBullets();
        }
    }

    public void SetBulletBarColor(Material _currentMaterial)
    {
        for(int i = 0; i < m_bulletsBar.Count; i++)
        {
            m_bulletsBar[i].color = _currentMaterial.color;
        }
    }

    public void DecreasedNumberOfBullets()
    {
        if(m_numberOfBullets > 0)
        {
            m_numberOfBullets--;
            DecreaseNumberOfBulletsBar();
        }
    }

    public void IncreaseNumberOfBullets()
    {
        if (m_numberOfBullets < m_maxNumberOfBullets)
        {
            m_numberOfBullets++;
            IncreaseNumberOfBulletsBar();
        }
    }

    public void DecreaseNumberOfBulletsBar()
    {
        for(int i = 0; i < m_bulletsBar.Count; i++)
        {
                if (m_bulletsBar[i].gameObject.activeInHierarchy)//if the bullet is active in hierachy then we should diminished it
                {
                    m_bulletsBar[i].gameObject.SetActive(false);
                    return;
                }
        }
    }

    public void IncreaseNumberOfBulletsBar()
    {
        for(int i = m_bulletsBar.Count - 1; i > 0; i--)
        {
                if (!m_bulletsBar[i].gameObject.activeInHierarchy)//if the bullet is active in hierachy then we should diminished it
                {
                    m_bulletsBar[i].gameObject.SetActive(true);
                    return;
                }
        }
    }

    
    // Update is called once per frame
    void FixedUpdate()
    {
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {

            m_rb.transform.Translate(m_forward * m_moveSpeed * Time.deltaTime);

            if (m_turnLerp)
            {
                this.RotatePlayerLerp();
            }
 

            // KMS
            m_kms += m_BASE_KM * Time.deltaTime;
            GameCtrlr.instance.SetKms(m_kms);
        }
        
    }

    private void Update() {
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {
            m_reloadDelayTracker += Time.deltaTime;
            if (m_reloadDelayTracker >= m_reloadDelay)
            {
                m_reloadDelayTracker = 0f;
                IncreaseNumberOfBullets();
            }
            // Change Color : For Test with Keyboard
            if (Input.GetKeyDown("space"))
            {
                GameCtrlr.instance.ChangeBulletColor();
            }
            if(!IsRolling() && !transform.GetComponentInChildren<Follower>().m_isTurning)    //verified if the player is not rolling
            {

                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    GotoLeft();
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    GotoRight();
                }
                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    Roll();
                }
            }

            // Touches
            //if(Input.touchCount == 1)
            //{
            //    Touch touch = Input.GetTouch(0);

            //    if(touch.phase == TouchPhase.Began)
            //    {
            //        m_lastPosition = touch.position;
            //    }
            //    if(touch.phase == TouchPhase.Ended)
            //    {
            //        // get the moved direction compared to the initial touch position
            //        Vector2 direction = touch.position - m_lastPosition ;

            //        // get the signed x direction
            //        if(direction.x >= 0) 
            //        {
            //            GotoRight();
            //        }
            //        else
            //        {
            //            GotoLeft();
            //        }

            //        float signedDirection = Mathf.Sign(direction.x);

            //        m_lastPosition = touch.position;
            //    }
            //}

            //SWIPE DETECTION

            if (Input.touchCount == 1) // user is touching the screen with a single touch
            {
                Touch touch = Input.GetTouch(0); // get the touch
                if (touch.phase == TouchPhase.Began) //check for the first touch
                {
                    m_fp = touch.position;
                    m_lp = touch.position;
                }
                else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
                {
                    m_lp = touch.position;
                }
                else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
                {
                    m_lp = touch.position;  //last touch position. Ommitted if you use list

                    //Check if drag distance is greater than 20% of the screen height
                    if ((Mathf.Abs(m_lp.x - m_fp.x) > m_dragDistance || Mathf.Abs(m_lp.y - m_fp.y) > m_dragDistance) && !IsRolling())
                    {//It's a drag
                     //check if the drag is vertical or horizontal
                        if (Mathf.Abs(m_lp.x - m_fp.x) > Mathf.Abs(m_lp.y - m_fp.y))
                        {   //If the horizontal movement is greater than the vertical movement...
                            if ((m_lp.x > m_fp.x))  //If the movement was to the right)
                            {   //Right swipe
                                Debug.Log("Right Swipe");
                                GotoRight();
                            }
                            else
                            {   //Left swipe
                                Debug.Log("Left Swipe");
                                GotoLeft();
                            }
                        }
                        else
                        {   //the vertical movement is greater than the horizontal movement
                            if (m_lp.y > m_fp.y)  //If the movement was up
                            {   //Up swipe
                                Debug.Log("Up Swipe");
                                Roll();
                            }
                            else
                            {   //Down swipe
                                Debug.Log("Down Swipe");
                                Roll();
                            }
                        }
                    }
                    else
                    {   //It's a tap as the drag distance is less than 20% of the screen height
                        Debug.Log("Tap");
                    }
                }
            }



        }


        
    }

    void GotoRight()
    {
        m_isSwipingRight = true;
        if (m_pos >= -1 && m_pos < 1 && !m_turnLerp)
        {
            StartCoroutine(ISwipe(1, 9.5f));
            m_pos += 1;
        }
        StartCoroutine(ResetSwipeRight());
    }

    void Roll()
    {
        StartCoroutine(AnimatorSetRoll(2.1f));
    }


    private IEnumerator AnimatorSetRoll(float animationLength)
    {
        this.transform.GetChild(0).GetComponent<Animator>().SetBool("ROLL", true);
       yield return new WaitForSeconds(animationLength);
        this.transform.GetChild(0).GetComponent<Animator>().SetBool("ROLL", false);
    }
    public bool IsRolling()
    {
        return this.transform.GetChild(0).GetComponent<Animator>().GetBool("ROLL");
    }

    void GotoLeft()
    {
        m_isSwipingLeft = true;
        if (m_pos <= 1 && m_pos > -1 && !m_turnLerp)
        {
            StartCoroutine(ISwipe(-1, 9.5f));
            m_pos -= 1;
        }
        StartCoroutine(ResetSwipeLeft());
    }

    IEnumerator ResetSwipeLeft()
    {
        yield return new WaitForSeconds(0.2f);
        m_isSwipingLeft = false;
    }
    
    IEnumerator ResetSwipeRight()
    {
        yield return new WaitForSeconds(0.2f);
        m_isSwipingRight = false;
    }

    IEnumerator ISwipe(int _dir = 1, float _max = 8.0f)
    {

       float progress = 0;

       while (progress < _max )
       {
            progress += (1.0f);

            if(_dir == 1)
                m_rb.transform.Translate(Vector3.right * progress);
            
            if(_dir == -1)
                m_rb.transform.Translate(Vector3.left * progress);
            

            if (progress >= _max )
            {
                break;
            }

            yield return 0;
       }

    }
     

    public void GetCoin(int type , int point) // 1 for Coin, 2 for Gem
    {
        switch (type)
        {
            case 1 : 
                m_coins += point;
                m_coinsText.text = m_coins.ToString();
                PlayerPrefs.SetInt("coins", m_coins);
            break;
            case 2 : 
                m_gems += point;
                m_gemsText.text = m_gems.ToString();
                PlayerPrefs.SetInt("gems", m_gems);
            break;
            case 3 : 
                m_collectedLives++;
                //GameCtrlr.instance.IncreaseHeartLoader(m_collectedLives);
                IncreaseHeartFillImage();
                if (m_collectedLives == 4)
                { 
                    // Debug.Log("Yay");
                    if(m_lives < 5)
                    {
                        m_lives++;
                        IncreaseLifeBar();
                        m_livesText.text = m_lives.ToString();
                        PlayerPrefs.SetInt("lives", m_lives);
                    }

                    m_collectedLives = 0;
                    //GameCtrlr.instance.IncreaseHeartLoader(m_collectedLives);
                    IncreaseHeartFillImage();
                }
                
            break;
            case 4 :  // Star
                GameCtrlr.instance.StartStarPowerup();
            break;
            default:break;
        }
        
    }

    public void DecreasePlayerLives()
    {
        StartCoroutine(IInjureEffect());

        m_lives--;
        DecreasePlayerLifeBar();
        if (m_lives > 0)
        {
            m_livesText.text = m_lives.ToString(); 
        }
        else
        {
            m_livesText.text = "0"; 
            // GameOver
            StartCoroutine(IWaitBeforeGameOver());
        }

    }

    public void InitHealthBar()
    {

        for (int i = 0; i < m_lifeBar.Count; i++)
        {
                m_lifeBar[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < GameCtrlr.instance.m_currentHealth.Get(); i++)
        {
                m_lifeBar[i].gameObject.SetActive(true);
                if(i == m_lifeBar.Count - 1)
                {
                    return;
                }
        }
    }

    public void DecreasePlayerLifeBar()
    {
        if(m_lives < MAX_HEALTH)
        {
            for (int i = 0; i < m_lifeBar.Count; i++)
            {
                if (m_lifeBar[i].gameObject.activeInHierarchy)
                {
                    m_lifeBar[i].gameObject.SetActive(false);
                    return;
                }
            }
        }
        else
        {
            int diff = m_lives - MAX_HEALTH;
            if(diff == 0)
            {
                m_healthBarNotif.SetActive(false);
            }
            else
            {
                SetHealthBarNotif();
            }
        }
    }

    public void IncreaseLifeBar()
    {
        for(int i = m_lifeBar.Count -1; i > 0; i--)
        {
            if(!m_lifeBar[i].gameObject.activeInHierarchy)
            {
                m_lifeBar[i].gameObject.SetActive(true);
                return;
            }
        }
    }

    public void IncreaseHeartFillImage()
    {
        if(m_heartFillStep < m_heartFillList.Count - 1)
        {
            m_heartFillStep++;
            m_heartImage.sprite = m_heartFillList[m_heartFillStep];
        }
        else
        {
            m_heartFillStep = 0;
            m_heartImage.sprite = m_heartFillList[m_heartFillStep];
        }
    }

    public void SaveScore()
    {
        PlayerPrefs.SetInt("coins", m_coins);
        PlayerPrefs.SetInt("gems", m_gems);
    }

    public void NextLevel(int lvl)
    {
        m_levelText.text = lvl.ToString();
        m_levelReachedText.text = lvl.ToString();
    }

    public void TurnPlayer(float angle)
    {   
        float angleDiff = angle + m_currentOrientationAngle ;
        int side = angleDiff > 0 ? -1 : 1;

        ORIENTATION newOrientation = m_orientation == ORIENTATION.STRAIGHT ? ORIENTATION.TURN : ORIENTATION.STRAIGHT;
        m_orientation = newOrientation;
        m_orientationSide = side;
        m_currentOrientationAngle = angleDiff;
        // Debug.Log("rota : " + angleDiff);
        // RotatePlayer(angleDiff, side);
        StartCoroutine(IToggleTurnLerp());
        // RotatePlayerLerp();
    }

    void RotatePlayerLerp()
    {
        Quaternion initRotation = m_rb.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation); 
        //Debug.Log(Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(0, m_rb.transform.localEulerAngles.y))) + " - " + m_currentOrientationAngle);
        //if(Mathf.Round(Mathf.Abs(Mathf.DeltaAngle(0, m_rb.transform.localEulerAngles.y))) >= Mathf.Abs(m_currentOrientationAngle) )
        if(Mathf.Approximately(Mathf.Abs(Quaternion.Dot(initRotation, finalRotation)), 1.0f))
        {
            m_pos = 0;
            m_turnLerp = false;
        }

        m_rb.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);

    }

    void SwipePlayerLerp()
    {
        Quaternion initRotation = m_rb.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        
        if(Mathf.Abs(m_rb.rotation.y) >= Mathf.Abs(m_currentOrientationAngle) )
            m_turnLerp = false;

        m_rb.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);

    }

   void RotateCameraLerp()
    {
        Quaternion initRotation = m_cam.transform.parent.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        
        if(Mathf.Abs(m_cam.transform.parent.rotation.y) >= Mathf.Abs(m_currentOrientationAngle) )
            m_turnCameraLerp = false;

        m_cam.transform.parent.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);
    }

    IEnumerator IToggleTurnLerp()
    {
        m_turnLerp = true;
        m_turnCameraLerp = true;
        yield return 0;
    }
    IEnumerator IInjureEffect()
    {
        GameCtrlr.instance.m_isCharacterBlinking = true;

        m_playerItem.transform.GetComponent<BoxCollider>().enabled = false;

        GameCtrlr.instance.ToggleMotionOnInjured(true); // Stop all motions

        int blinks = 2;
        
        StartCoroutine(IBlink(blinks));

        yield return 0;
    }
    IEnumerator IBlink(int _blinks)
    {   
        float blinkDuration = 0.15f;
        m_playerItem.SetActive(false);
        yield return new WaitForSeconds(blinkDuration);
        m_playerItem.SetActive(true);
        yield return new WaitForSeconds(blinkDuration);
        _blinks--;
        if(_blinks > 0)
        {
            StartCoroutine(IBlink(_blinks));
        }
        else
        {
            RestartAfterInjury();
        }
    }

    public int GetCurrentPos()
    {
        return m_pos;
    }

    void RestartAfterInjury()
    {
        // Debug.Log("Restarted...");
        m_playerItem.transform.GetComponent<BoxCollider>().enabled = true;

        GameCtrlr.instance.ToggleMotionOnInjured(false); // Restart all motions

        GameCtrlr.instance.m_isCharacterBlinking = false;

    }
    IEnumerator IWaitBeforeGameOver()
    {
        yield return new WaitForSeconds(0.5f);
        GameCtrlr.instance.GameOver();
    }
    
}

[System.Serializable]
public class CharacterPos
{
    public string positionName;
    public float positionValue;
}
