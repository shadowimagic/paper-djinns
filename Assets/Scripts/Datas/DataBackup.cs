﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtils;

public class DataBackup : Singleton<DataBackup>
{
    private const string BOSS_KEY = "UNLOCK_BOSS";

    public void InitBossKey(string _bossDisplayName)
    {
        string _key = BOSS_KEY + _bossDisplayName;
        if(!PlayerPrefs.HasKey(_key))
        {
            PlayerPrefs.SetInt(_key, 0);
        }
    }

    public void SetEarnedStarOnBossFight(string _bossDisplayName, int _earnedStars)
    {
        string _key = BOSS_KEY + _bossDisplayName;
        if(PlayerPrefs.GetInt(_key) < _earnedStars)
        {
            PlayerPrefs.SetInt(_key, _earnedStars);
        }
    }

    public bool HasUnlockedBoss(string _displayName)
    {
        string _key = BOSS_KEY + _displayName;

        if (PlayerPrefs.HasKey(_key))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int GetEarnedStarsOnBossFight(string _bossDisplayName)
    {
        string _key = BOSS_KEY + _bossDisplayName;

        if (PlayerPrefs.HasKey(_key))
        {
            return PlayerPrefs.GetInt(_key);
        }
        else
        {
            return 0;
        }
    }

}
