﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Dialog : MonoBehaviour
{
    public TextMeshProUGUI m_textDisplay;
    private int index = 0;
    public float m_typingSpeed;
    string m_savedText;
    Coroutine m_typeCoroutine;
    private void Start()
    {
        m_savedText = m_textDisplay.text;
        m_typeCoroutine = StartCoroutine(Type());
    }



    IEnumerator Type()
    {
        m_textDisplay.text = "";
            foreach (char letter in m_savedText.ToCharArray())
            {
                m_textDisplay.text += letter;
                yield return new WaitForSeconds(m_typingSpeed);
            }
    }

    public void StartTyping()
    {
        m_textDisplay.text = "";
        StopCoroutine(m_typeCoroutine);
        m_typeCoroutine = StartCoroutine(Type());
    }
}




