﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    public List<Dialog> m_dialogs;

    public void Type()
    {
        //should verified if the intro is activated
        foreach(Dialog d in m_dialogs)
        {
            d.StartTyping();
        }
    }
}
