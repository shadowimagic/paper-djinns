﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyConfigs : MonoBehaviour
{

    public bool m_isEnemyActivated;
    public int m_numberOfEnemies;
    public float m_enemySpeed;
    void Start()
    {
        if(m_isEnemyActivated)
        {
            ActivateEnemies();
        }
    }

    public void ActivateEnemies()
    {
        for(int i = 0; i < m_numberOfEnemies; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
            transform.GetChild(i).GetComponent<EnemyFollower>().speed = m_enemySpeed;
            DefineObstacle(i);
        }
    }


    public void DefineObstacle(int _index)
    {
        transform.GetChild(_index).gameObject.SetActive(true);
        //m_obstaclesIndexes.RemoveAt(_indexOfIndex);
        // Setting a material
        SetObstacleMaterial(transform.GetChild(_index));
    }

    void SetObstacleMaterial(Transform _obstacle)
    {
        int randomObstacleMaterialIndex = Random.Range(0, (GameCtrlr.instance.m_pickableObstaclesMaterials.Count));

        _obstacle.GetComponentInChildren<MeshRenderer>().sharedMaterial = GameCtrlr.instance.m_pickableObstaclesMaterials[randomObstacleMaterialIndex];
        string obstacleColorTag = GameCtrlr.instance.m_pickableObstaclesMaterials[randomObstacleMaterialIndex].name;
        string[] splitArray = obstacleColorTag.Split(char.Parse("_")); //
        obstacleColorTag = splitArray[1];
        _obstacle.gameObject.tag = obstacleColorTag;
        _obstacle.GetChild(0).gameObject.tag = obstacleColorTag;
        _obstacle.GetChild(0).GetComponent<ObstacleCtrlr>().SetTag(obstacleColorTag);

    }


}
