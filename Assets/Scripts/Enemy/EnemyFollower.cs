﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class EnemyFollower : MonoBehaviour
{
    public PathCreator pathToFollow;
    //speeds
    public float speed = 2;
    public float interval = 2;

    float distance; //DISTANCE TO NEXT PATH POINT

    //STATE MACHINES
    public enum EnemyStates
    {
        ON_PATH,
        IDLE
    }
    public EnemyStates enemyState;

    void FixedUpdate()
    {
        switch (enemyState)
        {
            case EnemyStates.ON_PATH:
                MoveOnThePath(pathToFollow);
                break;
            case EnemyStates.IDLE:

                break;
        }
    }

    void MoveOnThePath(PathCreator path)
    {

        distance += speed * Time.deltaTime;
        transform.position = path.path.GetPointAtDistance(distance + interval);
    }
}
